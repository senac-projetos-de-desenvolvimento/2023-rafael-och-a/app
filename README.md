<img src="https://gitlab.com/senac-projetos-de-desenvolvimento/2023-rafael-och-a/doc/-/wikis/uploads/899b60a7b9fc908cc2ae7fbfbddd8471/Wiki_logo.png" width="400" height="300">

<h1 align="center">Salva Obra - App</h1>
<p align="center">
<image align="center" src="./public/assets/readme/DwarfPost.png">
</p>
<p align="center"> Rafael Ochôa Mello - rafaelochoamello@gmail.com </p>

<p align="center">
 <a href="#sobre">Projeto base</a> •
 <a href="#objetivo">Objetivo</a> 
</p>

<p id="sobre">
  <h2> :memo: Sobre </h2>
  <p> Este repositório se objetiva a conter as implementações pertinentes ao App do projeto. O mesmo é realizado
  utilizando o framework Flutter, e a linguagem Dart. 
  </p>
  <p>
    Para detalhes mais pertinentes com a concepção geral do projeto, seus objetivos, motivações e processos
    arquiteturais, é recomendado a leitura da Wiki do mesmo. 
  </p>
  <p>
    Para rodar Para rodar um projeto, basta estar na pasta do mesmo e utilizar: flutter run.  
    É possível utilizar, além dos emuladores, o próprio dispositivo para rodar o projeto. 
  </p>
</p>

<p id="objetivo">
 <h2> :bow_and_arrow: Objetivo </h2>
  <p>
    Desenvolver uma aplicação funcional para a proposta apresentada ao projeto. Utilizar processos limpos
    de implementação e aplicação de uma arquitetura organizada para o mesmo. Buscando atomizar os componentes
    e aplicar ao máximo o conceito de responsabilidade única aos mesmos.
  </p>
  <p>
    A arquitetura escolhida para o mesmo é a 'Flutter Clean Architecture', buscando modularizar as responsabilidades
    do App em diretórios específicos.
  </p>

   <img src="https://media.licdn.com/dms/image/D4D22AQGUhBjnZdfQ6A/feedshare-shrink_2048_1536/0/1681058884905?e=1687392000&v=beta&t=0OvH2filL9itTptHD800qzfNjopXAi7UccG3RPxJZmY" width="600" height="500">
</p>
