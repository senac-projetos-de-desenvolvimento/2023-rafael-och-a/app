import 'package:salva_obra/utils/capitalize_fst_letter.dart';
import 'package:salva_obra/utils/work_categories.dart';

String categoryTitleAdjustment({required WorkCategories category}) {
  if (category != WorkCategories.frete) {
    return capitalizeFirstLetter(text: category.name);
  }
  return 'Fretes & \n Mudanças';
}
