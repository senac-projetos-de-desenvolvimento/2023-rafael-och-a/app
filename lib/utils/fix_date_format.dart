String fixDateFormat({required String date}) {
  String year = date.split('-')[0];
  String month = date.split('-')[1];
  String day = date.split('-')[2].split('T')[0];
  return '$day/$month/$year';
}
