import '../domain/entities/worker.dart';
import '../../domain/gbl_data.dart' as globals;

//Calcula a quantidade de prestadores próximos
//ao usuário
int calculateNearWorker({required List<Worker> wList}) {
  int near = 0;

  if (wList.isEmpty) return 0;
  for (final w in wList) {
    if (w.neighborhood == globals.neighborhood) near++;
  }

  return near;
}
