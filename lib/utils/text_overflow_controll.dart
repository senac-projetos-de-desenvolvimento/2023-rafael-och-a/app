String textOverflowControll({required int maxSize, required String text}) {
  return text.length > maxSize ? '${text.substring(0, maxSize)}...' : text;
}
