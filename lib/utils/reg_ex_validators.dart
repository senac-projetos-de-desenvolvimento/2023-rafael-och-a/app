class RegExValidators {
  const RegExValidators();

  bool validateEmail({required String email}) {
    return RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(email);
  }

  bool validateCPF({required String cpf}) {
    return RegExp(r'[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}').hasMatch(cpf);
  }

  bool validateCNPJ({required String cnpj}) {
    // ignore: unnecessary_new
    RegExp exp = new RegExp(
        r"([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})");

    return exp.hasMatch(cnpj);
  }
}
