import 'package:flutter/material.dart';
import '../../domain/gbl_data.dart' as globals;

void showGlobalsData() {
  debugPrint("----------------------------------------------");
  debugPrint("Dados armazenados no globals");
  debugPrint("ID :: ${globals.id}");
  debugPrint("UserName :: ${globals.userName}");
  debugPrint("State :: ${globals.uState}");
  debugPrint("City :: ${globals.city}");
  debugPrint("Neighborhood :: ${globals.neighborhood}");
  debugPrint("Is-Worker :: ${globals.isWorker}");
  debugPrint("Likes :: ${globals.likes}");
  debugPrint("Services :: ${globals.services}");
  debugPrint("Favorites :: ${globals.favorites}");
  debugPrint("----------------------------------------------");
}
