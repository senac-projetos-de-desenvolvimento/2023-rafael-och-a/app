import 'package:flutter/material.dart';

class RegistrationAreaTitle extends StatelessWidget {
  final String content;

  const RegistrationAreaTitle({super.key, required this.content});

  @override
  Widget build(BuildContext context) {
    final textScale = MediaQuery.of(context).textScaleFactor;

    return Text(
      content,
      textAlign: TextAlign.center,
      style: TextStyle(
        color: const Color.fromARGB(255, 97, 97, 97),
        fontSize: 20 * textScale,
        fontWeight: FontWeight.w600,
        shadows: const <Shadow>[
          Shadow(
            blurRadius: 4,
            offset: Offset(0, 4),
            color: Color.fromARGB(40, 0, 0, 0),
          )
        ],
      ),
    );
  }
}
