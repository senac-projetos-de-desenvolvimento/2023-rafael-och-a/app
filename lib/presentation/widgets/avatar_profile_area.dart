import 'package:flutter/material.dart';

class AvatarProfileArea extends StatelessWidget {
  final String avatar;
  final String name;
  final String city;
  final String state;

  const AvatarProfileArea(
      {super.key,
      required this.avatar,
      required this.name,
      required this.city,
      required this.state});

  @override
  Widget build(BuildContext context) {
    final textScale = MediaQuery.of(context).textScaleFactor;

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        //----------------------------------------------------------------------
        //Image
        Container(
          width: 120,
          height: 120,
          decoration: BoxDecoration(
            border: Border.all(color: const Color(0xffDDDDDD), width: 2),
            borderRadius: BorderRadiusDirectional.circular(60),
            color: const Color(0xff1E1F29),
          ),
          child: Align(
            alignment: Alignment.center,
            child: Image(
              fit: BoxFit.fill,
              image: AssetImage('lib/presentation/assets/${avatar}.png'),
            ),
          ),
        ),

        //----------------------------------------------------------------------
        //Name
        Padding(
          padding: const EdgeInsets.only(top: 12),
          child: Text(
            name,
            style: TextStyle(
              fontSize: 18 * textScale,
              fontWeight: FontWeight.w700,
              color: const Color(0xff222020),
            ),
          ),
        ),

        //----------------------------------------------------------------------
        //City'n State
        Text(
          '${city} / ${state}',
          style: TextStyle(
            fontSize: 14 * textScale,
            fontWeight: FontWeight.w600,
            // ignore: prefer_const_constructors
            color: Color(0xff616161),
          ),
        )
      ],
    );
  }
}
