import 'package:flutter/material.dart';

enum BtnType { defaultBtn, selectUserTypeBtn }

class Button extends StatefulWidget {
  final String btnText;
  final void Function() btnFunction;
  final BtnType btnType;

  const Button(
      {super.key,
      required this.btnText,
      required this.btnFunction,
      required this.btnType});

  @override
  State<Button> createState() => _Button();
}

class _Button extends State<Button> {
  @override
  Widget build(BuildContext context) {
    final scrWidth = MediaQuery.of(context).size.width;
    final scrHeight = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor;

    return OutlinedButton(
        onPressed: widget.btnFunction,
        style: OutlinedButton.styleFrom(
            backgroundColor: const Color(0xFF1E1F29),
            fixedSize: Size(
                widget.btnType == BtnType.defaultBtn
                    ? scrWidth * 0.78
                    : scrWidth * 0.68,
                widget.btnType == BtnType.defaultBtn
                    ? scrHeight * 0.0598
                    : scrHeight * 0.07),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5))),
        child: Text(
          widget.btnText,
          style: TextStyle(
              color: Colors.white,
              fontSize: 16 * textScale,
              fontWeight: FontWeight.w500),
        ));
  }
}
