import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:salva_obra/presentation/screens/worker_profile_screen.dart';

import '../screens/category_screen.dart';
import '../screens/profile_screen.dart';
import '../screens/services_history_screen.dart';

import '../../domain/gbl_data.dart' as globals;

enum HomeBtnType { workers, profile, services }

class CentralHomeButton extends StatelessWidget {
  final HomeBtnType btnType;
  const CentralHomeButton({super.key, required this.btnType});

  @override
  Widget build(BuildContext context) {
    final scrWidth = MediaQuery.of(context).size.width;
    final scrHeight = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor;

    void _changeScreen(BuildContext context) {
      print('tap');
      //Check System type
      Navigator.of(context).push(
        Platform.isAndroid
            ? MaterialPageRoute(
                builder: (_) {
                  if (btnType == HomeBtnType.workers) {
                    return const CategoryScreen();
                  } else if (btnType == HomeBtnType.services) {
                    return const ServicesHistoryScreen();
                  } else {
                    if (globals.isWorker == 1) {
                      return WorkerProfileScreen(id: globals.id);
                    }
                    return ProfileScreen(id: globals.id);
                  }
                },
              )
            : CupertinoPageRoute(
                builder: (_) {
                  if (btnType == HomeBtnType.workers) {
                    return const CategoryScreen();
                  } else if (btnType == HomeBtnType.services) {
                    return const ServicesHistoryScreen();
                  } else {
                    if (globals.isWorker == 1) {
                      return WorkerProfileScreen(id: globals.id);
                    }
                    return ProfileScreen(id: globals.id);
                  }
                },
              ),
      );
    }

    String loadBarTitle() {
      String title = '';
      if (btnType == HomeBtnType.workers) {
        title = 'Prestadores';
      } else if (btnType == HomeBtnType.services) {
        title = 'Meus serviços';
      } else if (btnType == HomeBtnType.profile) {
        title = 'Perfil';
      }
      return title;
    }

    String loadDescription() {
      String desc = '';
      if (btnType == HomeBtnType.workers) {
        desc = 'Contrate serviços de profissionais \n qualificados';
      } else if (btnType == HomeBtnType.services) {
        desc = 'Acompanhe os serviços que \n você contratou, ou está prestando';
      } else if (btnType == HomeBtnType.profile) {
        desc = 'Veja como seu perfil está sendo visto \n por outros usuários. ';
      }
      return desc;
    }

    return GestureDetector(
      onTap: () => {_changeScreen(context)},
      child: Card(
        color: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7)),
        child: SizedBox(
          width: scrWidth * 0.85,
          height: scrHeight * 0.095,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              //Left Side :: Box with icon -------------------------------------
              Container(
                height: scrHeight * 0.095,
                width: scrWidth * 0.17,
                alignment: Alignment.center,
                decoration: const BoxDecoration(
                  color: Color(0xff1E1F29),
                  borderRadius: BorderRadius.all(
                    Radius.circular(7),
                  ),
                ),
                child: Image(
                  width: scrWidth * 0.0875,
                  fit: BoxFit.contain,
                  image: AssetImage(
                      'lib/presentation/assets/icons/home_central_icons/${btnType.name}.png'),
                ),
              ),

              //Right Side :: Title'n description ------------------------------
              Padding(
                padding: const EdgeInsets.fromLTRB(11, 8, 0, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      loadBarTitle(),
                      style: TextStyle(
                          fontSize: 14 * textScale,
                          color: const Color(0xff222020),
                          fontWeight: FontWeight.w500),
                    ),
                    Text(
                      loadDescription(),
                      style: TextStyle(
                          fontSize: 8 * textScale,
                          fontWeight: FontWeight.w600,
                          color: const Color(0xff858798)),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
