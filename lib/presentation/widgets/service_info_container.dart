import 'package:flutter/material.dart';
import 'package:salva_obra/presentation/widgets/service_type_badge_widget.dart';
import 'package:salva_obra/utils/text_overflow_controll.dart';
import 'package:salva_obra/utils/work_categories.dart';

import '../../utils/capitalize_fst_letter.dart';

class ServiceInfoContainer extends StatelessWidget {
  final WorkCategories category;
  final String author;
  final String phone;
  final double price;
  final bool isProfit;
  final String date;

  const ServiceInfoContainer(
      {super.key,
      required this.category,
      required this.author,
      required this.phone,
      required this.price,
      required this.date,
      required this.isProfit});

  String loadButtonTitle() {
    if (category != WorkCategories.frete) {
      return capitalizeFirstLetter(text: category.name);
    }
    return 'Fretes & \nMudanças';
  }

  @override
  Widget build(BuildContext context) {
    final scrWidth = MediaQuery.of(context).size.width;
    final textScale = MediaQuery.of(context).textScaleFactor;

    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 0),
          child: SizedBox(
            width: scrWidth * 0.75,
            child: Row(
              //É aqui que está o problema, ajustar aqui....
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                //----------------------------------------------------------------------
                //Image
                Center(
                  child: ServiceTypeBadge(
                    category: category,
                    hasTitle: false,
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
                //----------------------------------------------------------------------
                //Central
                SizedBox(
                  width: 100,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        loadButtonTitle(),
                        style: TextStyle(
                          color: const Color(0xff1E1F29),
                          fontSize: 13 * textScale,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Text(
                        textOverflowControll(maxSize: 10, text: author),
                        softWrap: false,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: const Color(0xff717171),
                          fontSize: 10 * textScale,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      Text(
                        phone,
                        style: TextStyle(
                          color: const Color(0xff717171),
                          fontSize: 10 * textScale,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                ),
                //----------------------------------------------------------------------
                //Price + Date (update posterior)
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.local_atm,
                          size: 22,
                          color: isProfit
                              ? const Color.fromARGB(255, 48, 145, 87)
                              : const Color.fromARGB(255, 179, 75, 92),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 3),
                          child: Text(
                            isProfit
                                ? '+${price.toStringAsFixed(2)}'
                                : '-${price.toStringAsFixed(2)}',
                            style: TextStyle(
                              color: isProfit
                                  ? const Color(0xff5DD88E)
                                  : const Color(0xffD85D73),
                              fontSize: 11 * textScale,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8),
                      child: Text(
                        date,
                        style: TextStyle(
                          color: const Color(0xff717171),
                          fontSize: 10 * textScale,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        const Divider()
      ],
    );
  }
}
