import 'package:flutter/material.dart';
import 'package:salva_obra/presentation/widgets/work_categories_register/category_register_button_widget.dart';

class CategoryRegister extends StatefulWidget {
  final bool isLoadingData;
  final Set selectedCategories;
  CategoryRegister(
      {super.key,
      required this.isLoadingData,
      required this.selectedCategories});

  @override
  State<CategoryRegister> createState() => _CategoryRegister();
}

class _CategoryRegister extends State<CategoryRegister> {
  // Set _selectedCategories = new Set();
  // Set get selectedCategories => _selectedCategories;

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          //Col-1
          Padding(
            padding: const EdgeInsets.only(right: 5),
            child: Column(children: <Widget>[
              CategoryRegisterButton(
                name: 'Servente',
                setData: widget.selectedCategories,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: CategoryRegisterButton(
                  name: 'Arquitetura',
                  setData: widget.selectedCategories,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: CategoryRegisterButton(
                  name: 'Encanamento',
                  setData: widget.selectedCategories,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: CategoryRegisterButton(
                  name: 'Marcenaria',
                  setData: widget.selectedCategories,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: CategoryRegisterButton(
                  name: 'Demolição',
                  setData: widget.selectedCategories,
                ),
              ),
            ]),
          ),
          //Col-2
          Padding(
            padding: const EdgeInsets.only(left: 5),
            child: Column(children: <Widget>[
              CategoryRegisterButton(
                name: 'Pedreiro',
                setData: widget.selectedCategories,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: CategoryRegisterButton(
                  name: 'Engenharia',
                  setData: widget.selectedCategories,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: CategoryRegisterButton(
                  name: 'Elétrica',
                  setData: widget.selectedCategories,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: CategoryRegisterButton(
                  name: 'Fretes',
                  setData: widget.selectedCategories,
                ),
              ),

              //Util-Somente para ver os dados
              // ElevatedButton(
              //     onPressed: () => {print(widget.selectedCategories)},
              //     child: Text('Dados'))
            ]),
          ),
        ]);
  }
}
