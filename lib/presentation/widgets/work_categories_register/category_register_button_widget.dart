import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class CategoryRegisterButton extends StatefulWidget {
  final String name;
  final Set setData;

  const CategoryRegisterButton(
      {super.key, required this.name, required this.setData});

  @override
  State<CategoryRegisterButton> createState() => _CategoryRegisterButton();
}

class _CategoryRegisterButton extends State<CategoryRegisterButton> {
  bool _isSelected = false;

  @override
  Widget build(BuildContext context) {
    final scrWidth = MediaQuery.of(context).size.width;
    final scrHeight = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor;

    void _handleSetData() {
      if (_isSelected) {
        widget.setData.add(widget.name);
      } else {
        if (widget.setData.contains(widget.name)) {
          widget.setData.remove(widget.name);
        }
      }
    }

    void _updateSelection() {
      setState(() {
        _isSelected = !_isSelected;
        _handleSetData();
      });
    }

    return Material(
      elevation: 5,
      shadowColor: const Color.fromARGB(118, 0, 0, 64),
      borderRadius: BorderRadius.circular(5),
      //Button
      child: OutlinedButton(
        onPressed: _updateSelection,
        style: OutlinedButton.styleFrom(
            side: BorderSide(
                color: _isSelected
                    ? const Color(0xffffffff)
                    : const Color(0xffDDDDDD)),
            backgroundColor:
                _isSelected ? const Color(0xff5F6174) : const Color(0xffFFFFFF),
            fixedSize: Size(scrWidth * 0.37, scrHeight * 0.079),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5))),
        child: Text(
          widget.name,
          style: TextStyle(
            color:
                _isSelected ? const Color(0xffFFFFFF) : const Color(0xff757893),
            fontSize: 16 * textScale,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}
