import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:salva_obra/presentation/screens/worker_list_neighborhood_screen.dart';

//Importar aqui a tela de listagem de prestadores do bairro

class WorkerListInfoBar extends StatelessWidget {
  final String appBarTitle;
  final int amount;
  final int near;
  final int category;

  const WorkerListInfoBar({
    super.key,
    required this.amount,
    required this.near,
    required this.category,
    required this.appBarTitle,
  });

  void _changeScreen(BuildContext context) {
    //Check System type
    Navigator.of(context).push(Platform.isAndroid
        ? MaterialPageRoute(builder: (_) {
            return WorkerListNeighborhoodScreen(
                appBarTitle: appBarTitle, categoryId: category);
          })
        : CupertinoPageRoute(builder: (_) {
            return WorkerListNeighborhoodScreen(
                appBarTitle: appBarTitle, categoryId: category);
          }));
  }

  @override
  Widget build(BuildContext context) {
    final scrWidth = MediaQuery.of(context).size.width;
    final scrHeight = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor;

    return Card(
      color: const Color(0xff1E1F29),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7)),
      child: SizedBox(
        width: scrWidth * 0.85,
        height: scrHeight * 0.10,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            //Services --------------------------------------------------
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  amount.toString(),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 24 * textScale,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Text(
                  'Encontrados',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 12 * textScale,
                    fontWeight: FontWeight.w400,
                  ),
                )
              ],
            ),

            //Favorites --------------------------------------------------
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                GestureDetector(
                  onTap: () => {_changeScreen(context)},
                  child: Column(
                    children: [
                      Text(
                        near.toString(),
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 24 * textScale,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Text(
                        'No seu bairro',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 12 * textScale,
                          fontWeight: FontWeight.w400,
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
