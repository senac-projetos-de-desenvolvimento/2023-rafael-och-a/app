import 'package:flutter/material.dart';
import 'package:salva_obra/utils/capitalize_fst_letter.dart';
import 'package:salva_obra/utils/work_categories.dart';

class ServiceTypeBadge extends StatelessWidget {
  final WorkCategories category;
  final bool hasTitle;
  const ServiceTypeBadge(
      {super.key, required this.category, required this.hasTitle});

  String loadButtonTitle() {
    if (category != WorkCategories.frete) {
      return capitalizeFirstLetter(text: category.name);
    }
    return 'Fretes & \n Mudanças';
  }

  @override
  Widget build(BuildContext context) {
    final textScale = MediaQuery.of(context).textScaleFactor;
    final scrWidth = MediaQuery.of(context).size.width;
    final scrHeight = MediaQuery.of(context).size.height;

    return Column(
      children: <Widget>[
        //----------------------------------------------------------------------
        //Icon -----------------------------------------------------------------
        Container(
          width: 70,
          height: 70,
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 1,
                blurRadius: 4,
                offset: const Offset(0, 3),
              ),
            ],
            border: Border.all(color: const Color(0xffDDDDDD), width: 2),
            borderRadius: BorderRadiusDirectional.circular(50),
            color: const Color(0xff1E1F29),
          ),
          child: Align(
            alignment: Alignment.center,
            child: Image(
              width: 32,
              fit: BoxFit.contain,
              image: AssetImage(
                  'lib/presentation/assets/icons/categories_icons/${category.name}.png'),
            ),
          ),
        ),
        //----------------------------------------------------------------------
        //Text -----------------------------------------------------------------
        hasTitle
            ? Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Text(
                  loadButtonTitle(),
                  style: TextStyle(
                    fontSize: 12 * textScale,
                    color: const Color(0xff1E1F29),
                    fontWeight: FontWeight.w600,
                  ),
                ),
              )
            : Container(),
      ],
    );
  }
}
