import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:salva_obra/presentation/screens/worker_profile_screen.dart';
import 'package:salva_obra/utils/text_overflow_controll.dart';

class WorkerMiniData extends StatelessWidget {
  final int id;
  final String title;
  final String adress;
  final String phone;
  final int likes;

  const WorkerMiniData({
    super.key,
    required this.title,
    required this.adress,
    required this.phone,
    required this.likes,
    required this.id,
  });

  void _changeScreen(BuildContext context) {
    //Check System type
    Navigator.of(context).push(Platform.isAndroid
        ? MaterialPageRoute(builder: (_) {
            return WorkerProfileScreen(id: id);
          })
        : CupertinoPageRoute(builder: (_) {
            return WorkerProfileScreen(id: id);
          }));
  }

  @override
  Widget build(BuildContext context) {
    final scrWidth = MediaQuery.of(context).size.width;
    final scrHeight = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor;

    return GestureDetector(
      onTap: () => {_changeScreen(context)},
      child: SizedBox(
        width: scrWidth * 0.76,
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 45,
                  height: 46,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      border: Border.all(color: Colors.black)),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      textOverflowControll(maxSize: 15, text: title),
                      style: TextStyle(
                        fontSize: 13 * textScale,
                        fontWeight: FontWeight.w600,
                        color: const Color(0xff1E1F29),
                      ),
                    ),
                    Text(
                      textOverflowControll(maxSize: 22, text: adress),
                      style: TextStyle(
                        fontSize: 10 * textScale,
                        fontWeight: FontWeight.w600,
                        color: const Color(0xff717171),
                      ),
                    ),
                    Text(
                      phone,
                      style: TextStyle(
                        fontSize: 10 * textScale,
                        fontWeight: FontWeight.w600,
                        color: const Color(0xff717171),
                      ),
                    )
                  ],
                ),
                Text(
                  '${likes.toString()} curtidas',
                  style: TextStyle(
                      fontSize: 10 * textScale,
                      fontWeight: FontWeight.w800,
                      color: const Color(0xff575865)),
                )
              ],
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 18),
              child: Divider(),
            )
          ],
        ),
      ),
    );
  }
}
