import 'package:flutter/material.dart';

class InfoBar extends StatelessWidget {
  final bool isWorkerType;
  final int likes, favorites, services;

  const InfoBar(
      {super.key,
      required this.isWorkerType,
      required this.likes,
      required this.favorites,
      required this.services});

  @override
  Widget build(BuildContext context) {
    final scrWidth = MediaQuery.of(context).size.width;
    final scrHeight = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor;

    return Card(
      color: const Color(0xff1E1F29),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7)),
      child: SizedBox(
        width: scrWidth * 0.85,
        height: scrHeight * 0.10,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            //Services --------------------------------------------------
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  services.toString(),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 24 * textScale,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Text(
                  'Serviços',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 12 * textScale,
                    fontWeight: FontWeight.w400,
                  ),
                )
              ],
            ),

            //Likes -------------------------------------------------------
            isWorkerType
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        likes.toString(),
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 24 * textScale,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Text(
                        'Likes',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 12 * textScale,
                          fontWeight: FontWeight.w400,
                        ),
                      )
                    ],
                  )
                : Container(),

            //Favorites --------------------------------------------------
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  favorites.toString(),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 24 * textScale,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Text(
                  'Favoritos',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 12 * textScale,
                    fontWeight: FontWeight.w400,
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
