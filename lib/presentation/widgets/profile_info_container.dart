import 'package:flutter/material.dart';
import 'package:salva_obra/presentation/widgets/service_type_badge_widget.dart';
import 'package:salva_obra/utils/work_categories.dart';

class ProfileInfoContainer extends StatelessWidget {
  final bool isWorker;
  final String email;
  final String phoneA;
  final String phoneB;
  final String cep;
  final String adress;
  final String neighborhood;
  final String description;

  const ProfileInfoContainer(
      {super.key,
      required this.email,
      required this.phoneA,
      required this.phoneB,
      required this.cep,
      required this.adress,
      required this.neighborhood,
      required this.description,
      required this.isWorker});

  @override
  Widget build(BuildContext context) {
    final scrWidth = MediaQuery.of(context).size.width;
    final scrHeight = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(height: 20),
        //----------------------------------------------------------------------
        //WorkCategories
        //Vai ter que fazer uma query pra pegar as categorias quando carregar a página
        Text(
          'Serviços Prestados',
          style: TextStyle(
            fontSize: 16 * textScale,
            fontWeight: FontWeight.w600,
            color: const Color(0xff1E1F29),
          ),
        ),
        isWorker
            ? Wrap(
                children: const <Widget>[
                  Padding(
                    padding: EdgeInsets.all(10),
                    child: ServiceTypeBadge(
                        category: WorkCategories.demolicao, hasTitle: true),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10),
                    child: ServiceTypeBadge(
                        category: WorkCategories.pedreiro, hasTitle: true),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10),
                    child: ServiceTypeBadge(
                        category: WorkCategories.servente, hasTitle: true),
                  ),
                ],
              )
            : SizedBox(width: 0, height: 0),
        const SizedBox(height: 20, width: 0),
        //----------------------------------------------------------------------
        //E-mail + Phone
        Text(
          'Contato',
          style: TextStyle(
            fontSize: 16 * textScale,
            fontWeight: FontWeight.w600,
            color: const Color(0xff1E1F29),
          ),
        ),
        Container(
          width: scrWidth * 0.70,
          decoration: BoxDecoration(
              border: Border.all(
                color: const Color(0xffDDDDDD),
              ),
              borderRadius: BorderRadius.circular(5)),
          child: Center(
            child: Column(children: <Widget>[
              SizedBox(
                height: 40,
                child: Row(
                  children: [
                    const Padding(
                      padding: EdgeInsets.fromLTRB(14, 0, 0, 0),
                      child: Icon(Icons.email),
                    ),
                    const SizedBox(width: 20),
                    Flexible(
                      child: Text(
                        email,
                      ),
                    ),
                  ],
                ),
              ),
              const Divider(),
              SizedBox(
                height: 40,
                child: Row(
                  children: [
                    const Padding(
                      padding: EdgeInsets.fromLTRB(14, 0, 0, 0),
                      child: Icon(Icons.phone_android),
                    ),
                    const SizedBox(width: 20),
                    Text(phoneA),
                  ],
                ),
              ),
              isWorker && phoneB.isNotEmpty
                  ? Column(
                      children: [
                        const Divider(),
                        SizedBox(
                          height: 40,
                          child: Row(
                            children: [
                              const Padding(
                                padding: EdgeInsets.fromLTRB(14, 0, 0, 0),
                                child: Icon(Icons.phone_android),
                              ),
                              const SizedBox(width: 20),
                              Text(phoneB),
                            ],
                          ),
                        ),
                      ],
                    )
                  : const SizedBox(
                      width: 0,
                      height: 0,
                    ),
            ]),
          ),
        ),
        SizedBox(height: 25),
        Text(
          'Endereço',
          style: TextStyle(
            fontSize: 16 * textScale,
            fontWeight: FontWeight.w600,
            color: const Color(0xff1E1F29),
          ),
        ),
        //----------------------------------------------------------------------
        //Adress Info
        Container(
          width: scrWidth * 0.70,
          decoration: BoxDecoration(
              border: Border.all(
                color: const Color(0xffDDDDDD),
              ),
              borderRadius: BorderRadius.circular(5)),
          child: Column(children: <Widget>[
            Container(
              height: 40,
              child: Row(
                children: [
                  const Padding(
                    padding: EdgeInsets.fromLTRB(14, 0, 0, 0),
                    child: Icon(Icons.location_on),
                  ),
                  const SizedBox(width: 20),
                  Text(cep),
                ],
              ),
            ),
            const Divider(),
            Container(
                height: 40,
                child: Row(
                  children: [
                    const Padding(
                      padding: EdgeInsets.fromLTRB(14, 0, 0, 0),
                      child: Icon(Icons.map),
                    ),
                    const SizedBox(width: 20),
                    Flexible(child: Text(adress)),
                  ],
                )),
            const Divider(),
            Container(
              height: 40,
              child: Row(
                children: [
                  const Padding(
                    padding: EdgeInsets.fromLTRB(14, 0, 0, 0),
                    child: Icon(Icons.signpost),
                  ),
                  const SizedBox(width: 20),
                  Flexible(child: Text('Bairro:${neighborhood}')),
                ],
              ),
            ),
          ]),
        ),
        const SizedBox(height: 25),
        //----------------------------------------------------------------------
        //Description
        isWorker
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Descrição',
                    style: TextStyle(
                      fontSize: 16 * textScale,
                      fontWeight: FontWeight.w600,
                      color: const Color(0xff1E1F29),
                    ),
                  ),
                  //----------------------------------------------------------------------
                  //Adress Info
                  Container(
                    width: scrWidth * 0.70,
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: const Color(0xffDDDDDD),
                        ),
                        borderRadius: BorderRadius.circular(5)),
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 15, vertical: 20),
                      child: Flexible(child: Text(description)),
                    )),
                  ),
                ],
              )
            : SizedBox(width: 0, height: 0),

        const SizedBox(height: 40),
      ],
    );
  }
}
