import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:salva_obra/utils/work_categories.dart';
import 'package:salva_obra/utils/capitalize_fst_letter.dart';
import 'package:salva_obra/presentation/screens/worker_list_screen.dart';

class CategoryButton extends StatelessWidget {
  final WorkCategories category;
  final int categoryId;
  const CategoryButton({
    super.key,
    required this.category,
    required this.categoryId,
  });

  @override
  Widget build(BuildContext context) {
    final scrWidth = MediaQuery.of(context).size.width;
    final scrHeight = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor;

    String loadButtonTitle() {
      if (category != WorkCategories.frete) {
        return capitalizeFirstLetter(text: category.name);
      }
      return 'Fretes & \n Mudanças';
    }

    void _changeScreen(BuildContext context) {
      //Check System type
      Navigator.of(context).push(
        Platform.isAndroid
            ? MaterialPageRoute(builder: (_) {
                return WorkerListScreen(
                  appBarTitle: loadButtonTitle(),
                  workCat: category,
                  categoryId: categoryId,
                );
              })
            : CupertinoPageRoute(
                builder: (_) {
                  return WorkerListScreen(
                    appBarTitle: loadButtonTitle(),
                    workCat: category,
                    categoryId: categoryId,
                  );
                },
              ),
      );
    }

    return Padding(
      padding: const EdgeInsets.only(top: 13),
      child: OutlinedButton(
        onPressed: () => _changeScreen(context),
        style: OutlinedButton.styleFrom(
          fixedSize: const Size(120, 140),
          side: const BorderSide(color: Color(0xffDDDDDD)),
          backgroundColor: const Color(0xff1E1F29),
          elevation: 2,
          shadowColor: const Color.fromARGB(255, 124, 124, 124),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            // -------------------------------------------------------------------
            //Title --------------------------------------------------------------
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 0),
              child: Text(
                loadButtonTitle(),
                style: TextStyle(
                  fontSize: 11.5 * textScale,
                  fontWeight: FontWeight.w600,
                  color: Colors.white,
                ),
              ),
            ),
            // -------------------------------------------------------------------
            //Icon ---------------------------------------------------------------
            Image(
              width: 68,
              height: 64,
              fit: BoxFit.contain,
              image: AssetImage(
                  'lib/presentation/assets/icons/categories_icons/${category.name}.png'),
            ),
          ],
        ),
      ),
    );
  }
}
