import 'package:flutter/material.dart';
import 'package:salva_obra/domain/repositories/worker_handler.dart';
import 'package:salva_obra/presentation/widgets/worker_list_info_bar.dart';
import 'package:salva_obra/presentation/widgets/worker_mini_data.dart';
import 'package:salva_obra/utils/calculate_near_worker.dart';
import 'package:salva_obra/utils/work_categories.dart';

import '../../domain/entities/worker.dart';

class WorkerListScreen extends StatefulWidget {
  final String appBarTitle;
  final WorkCategories workCat;
  final int categoryId;

  const WorkerListScreen(
      {super.key,
      required this.appBarTitle,
      required this.workCat,
      required this.categoryId});

  @override
  State<WorkerListScreen> createState() => _WorkerListScreen();
}

class _WorkerListScreen extends State<WorkerListScreen> {
  late Future<List<Worker>> workers;

  @override
  void initState() {
    super.initState();
    workers = WorkerHandler().listWorkersByCat(categoryId: widget.categoryId);
    debugPrint('\n------------------------------------------\n');
    debugPrint('Lista de prestadores :: ${workers.toString()}');
  }

  @override
  Widget build(BuildContext context) {
    final scrWidth = MediaQuery.of(context).size.width;
    final scrHeight = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor;

    return Container(
      decoration: const BoxDecoration(
        color: Color.fromARGB(255, 255, 255, 255), // background color
        image: DecorationImage(
            image: AssetImage('lib/presentation/assets/screen_bg.png'),
            fit: BoxFit.cover), // background image above color
      ),
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: const Color(0xff1E1F29),
            shadowColor: const Color(0xff1F1F1F),
            iconTheme: const IconThemeData(color: Color(0xff8F8F90), size: 30),
            centerTitle: true,
            title: Text(
              widget.appBarTitle,
              style: TextStyle(
                color: const Color(0xffF5F5F5),
                fontSize: 20 * textScale,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          backgroundColor: Colors.transparent,
          body: ListView(
            children: <Widget>[
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        Center(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 50),
                            child: Container(
                              width: scrWidth * 0.81,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(
                                    color: const Color.fromARGB(
                                        255, 224, 224, 224),
                                    width: 1),
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(7),
                                ),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                //----------------------------------------------
                                //Workers List ---------------------------------
                                children: <Widget>[
                                  const SizedBox(
                                    height: 88,
                                  ),
                                  //--------------------------------------------
                                  //FutureBuilder
                                  FutureBuilder(
                                    future: workers,
                                    builder: (context, snapshot) {
                                      if (snapshot.connectionState ==
                                          ConnectionState.done) {
                                        if (snapshot.hasError) {
                                          debugPrint(snapshot.error.toString());

                                          return const Text("ERRO");
                                        }

                                        return SizedBox(
                                          width: scrWidth,
                                          height: scrHeight,
                                          child: ListView.builder(
                                              itemCount: snapshot.data!.length,
                                              itemBuilder: (context, index) {
                                                return WorkerMiniData(
                                                  id: snapshot.data![index].id,
                                                  title: snapshot
                                                      .data![index].title,
                                                  adress: snapshot
                                                      .data![index].adress,
                                                  likes: snapshot
                                                      .data![index].likes,
                                                  phone: snapshot
                                                      .data![index].phoneA,
                                                );
                                              }),
                                        );
                                      } else {
                                        //Aqui também podemos usar um if
                                        //de ConnectionState.waiting
                                        return const CircularProgressIndicator();
                                      }
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Center(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 25),
                            child: FutureBuilder(
                              future: workers,
                              builder: (context, snapshot) {
                                if (snapshot.connectionState ==
                                    ConnectionState.done) {
                                  if (snapshot.hasError) {
                                    debugPrint(snapshot.error.toString());
                                    return const Text("Error");
                                  }

                                  return WorkerListInfoBar(
                                    appBarTitle: widget.appBarTitle,
                                    amount: snapshot.data!.length,
                                    near: calculateNearWorker(
                                        wList: snapshot.data!),
                                    category: widget.categoryId,
                                  );
                                } else {
                                  return const CircularProgressIndicator();
                                }
                              },
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
