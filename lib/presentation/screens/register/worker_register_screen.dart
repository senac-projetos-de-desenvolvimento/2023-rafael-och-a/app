import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:salva_obra/domain/repositories/worker_handler.dart';
import 'package:salva_obra/presentation/screens/register/register_confirmation_screen.dart';
import 'package:salva_obra/presentation/widgets/work_categories_register/category_register_widget.dart';
import 'package:salva_obra/utils/body_resp_string_handler.dart';
import 'package:salva_obra/utils/reg_ex_validators.dart';
import 'dart:io';

import '../../../domain/entities/worker.dart';
import '../../widgets/button_widget.dart';
import '../../widgets/registration_area_title.dart';

class WorkerRegister extends StatefulWidget {
  const WorkerRegister({super.key});

  @override
  State<WorkerRegister> createState() => _WorkerRegister();
}

class _WorkerRegister extends State<WorkerRegister> {
  final _formKey = GlobalKey<FormState>();
  final Set selectedCategories = new Set();

  final validators = const RegExValidators();

  String _passInput = '';
  String _confPassInput = '';
  String _email = '';
  String _confEmail = '';

  String _title = '';
  String _CNPJ = '';
  String _CPF = '';
  String _phoneA = '';
  String _phoneB = '';
  String _adress = '';
  String _neighborhood = '';
  String _city = '';
  String _cep = '';
  String _cState = 'RS';
  String _description = '';

  //Late - API Addition controller
  late Worker newWorker;

  void _changeScreen(BuildContext context) {
    //Check System type
    Navigator.of(context).pushReplacement(Platform.isAndroid
        ? MaterialPageRoute(builder: (_) {
            return const RegisterConfirmationScreen(isClient: false);
          })
        : CupertinoPageRoute(builder: (_) {
            return const RegisterConfirmationScreen(isClient: false);
          }));
  }

  void _updateDropDownState(String? value) {
    if (value is String) {
      setState(() {
        _cState = value;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final scrWidth = MediaQuery.of(context).size.width;
    final scrHeight = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor;

    return Container(
      decoration: const BoxDecoration(
        color: Color.fromARGB(255, 255, 255, 255), // background color
        image: DecorationImage(
            image: AssetImage('lib/presentation/assets/screen_bg.png'),
            fit: BoxFit.cover), // background image above color
      ),
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            shadowColor: Colors.transparent,
            iconTheme: const IconThemeData(color: Color(0xff8F8F90), size: 30),
            centerTitle: true,
            title: Text(
              'Prestador',
              style: TextStyle(
                  color: const Color(0xff4D4D4D),
                  fontSize: 24 * textScale,
                  fontWeight: FontWeight.w600,
                  shadows: const <Shadow>[
                    Shadow(
                        blurRadius: 4,
                        offset: Offset(0, 4),
                        color: Color.fromARGB(60, 0, 0, 0))
                  ]),
            ),
          ),
          backgroundColor: Colors.transparent,
          body: ListView(
            children: <Widget>[
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    const Padding(
                      padding: EdgeInsets.fromLTRB(0, 50, 0, 31),
                      child: RegistrationAreaTitle(
                          content: 'Informações da conta'),
                    ),
                    //User form
                    Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          //----
                          //E-mail
                          Padding(
                            padding: const EdgeInsets.only(top: 5),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Favor preencher os dados';
                                    } else if (value.length < 7) {
                                      return 'Dados inválidos, mínimo 7 caracteres!';
                                    }
                                    if (validators.validateEmail(
                                            email: _email) ==
                                        false) {
                                      return 'Informe um e-mail válido';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _email = value;
                                    });
                                  },
                                  keyboardType: TextInputType.emailAddress,
                                  decoration: InputDecoration(
                                    labelText: 'E-mail',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          //----
                          //Conf.E-mail
                          Padding(
                            padding: const EdgeInsets.only(top: 13),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Favor preencher os dados';
                                    } else if (value.length < 7) {
                                      return 'Dados inválidos, mínimo 7 caracteres!';
                                    } else if (_confEmail != _email) {
                                      return 'Favor preencher com o mesmo e-mail';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _confEmail = value;
                                    });
                                  },
                                  keyboardType: TextInputType.emailAddress,
                                  decoration: InputDecoration(
                                    labelText: 'Confirmar E-mail',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          //--------
                          //Password
                          Padding(
                            padding: const EdgeInsets.only(top: 33),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Favor preencher os dados';
                                    } else if (value.length < 6) {
                                      return 'Senha inválida, mínimo 6 caracteres!';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _passInput = value;
                                    });
                                  },
                                  obscureText: true,
                                  decoration: InputDecoration(
                                    labelText: 'Senha',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          //-----------
                          //Conf.Password
                          Padding(
                            padding: const EdgeInsets.only(top: 13),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Favor preencher os dados';
                                    } else if (value.length < 6) {
                                      return 'Senha inválida, mínimo 6 caracteres!';
                                    } else if (_confPassInput != _passInput) {
                                      return 'As senhas devem ser iguais.';
                                    }

                                    return null;
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _confPassInput = value;
                                    });
                                  },
                                  obscureText: true,
                                  decoration: InputDecoration(
                                    labelText: 'Confirmar senha',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          const Padding(
                            padding: EdgeInsets.fromLTRB(0, 50, 0, 31),
                            child: RegistrationAreaTitle(
                                content: 'Informações profissionais'),
                          ),

                          //------
                          //F.Name
                          Padding(
                            padding: const EdgeInsets.only(top: 13),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Favor preencher os dados';
                                    } else if (value.length < 2) {
                                      return 'Dados inválidos, mínimo 2 caracteres!';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _title = value;
                                    });
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'Nome da empresa / Prestador',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          //------
                          //CNPJ
                          Padding(
                            padding: const EdgeInsets.only(top: 13),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return null;
                                    } else {
                                      if (validators.validateCNPJ(
                                              cnpj: value) ==
                                          false) {
                                        return 'Favor preencher um CNPJ válido';
                                      }
                                      return null;
                                    }
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _CNPJ = value;
                                    });
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'CNPJ (Opcional)',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          //------
                          //CPF
                          Padding(
                            padding: const EdgeInsets.only(top: 13),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return null;
                                    } else {
                                      if (validators.validateCPF(cpf: value) ==
                                          false) {
                                        return 'Favor preencher um CPF válido';
                                      }
                                      return null;
                                    }
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _CPF = value;
                                    });
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'CPF (Opcional)',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          //----
                          //Phone - A
                          Padding(
                            padding: const EdgeInsets.only(top: 33),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Favor preencher os dados';
                                    } else if (value.length < 9 ||
                                        value.length > 12) {
                                      return 'Numero de telefone inválido.';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _phoneA = value;
                                    });
                                  },
                                  keyboardType: TextInputType.phone,
                                  decoration: InputDecoration(
                                    labelText: 'Telefone - 1',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          //----
                          //Phone - B
                          Padding(
                            padding: const EdgeInsets.only(top: 13),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return null;
                                    } else if (value.length < 9 ||
                                        value.length > 11) {
                                      return 'Numero de telefone inválido.';
                                    } else {
                                      return null;
                                    }
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _phoneB = value;
                                    });
                                  },
                                  keyboardType: TextInputType.phone,
                                  decoration: InputDecoration(
                                    labelText: 'Telefone - 2 (Opcional)',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          //------
                          //Adress
                          Padding(
                            padding: const EdgeInsets.only(top: 33),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Favor preencher os dados';
                                    } else if (value.length < 5) {
                                      return 'Dados inválidos, mínimo 5 caracteres!';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _adress = value;
                                    });
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'Endereço',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          //------
                          //Neighborhood
                          Padding(
                            padding: const EdgeInsets.only(top: 13),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Favor preencher os dados';
                                    } else if (value.length < 2) {
                                      return 'Dados inválidos, mínimo 2 caracteres!';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _neighborhood = value;
                                    });
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'Bairro',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          //------
                          //City
                          Padding(
                            padding: const EdgeInsets.only(top: 13),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Favor preencher os dados';
                                    } else if (value.length < 2) {
                                      return 'Dados inválidos, mínimo 2 caracteres!';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _city = value;
                                    });
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'Cidade',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          //----------
                          //CEP & State
                          SizedBox(
                            width: scrWidth * 0.78,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                //------
                                //CEP
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(0, 33, 15, 33),
                                  child: Material(
                                    elevation: 5,
                                    shadowColor: const Color(0xff000040),
                                    borderRadius: BorderRadius.circular(5),
                                    child: SizedBox(
                                      width: scrWidth * 0.43,
                                      height: scrHeight * 0.06,
                                      child: TextFormField(
                                        keyboardType: TextInputType.number,
                                        validator: (value) {
                                          if (value == null || value.isEmpty) {
                                            return 'Favor preencher os dados';
                                          } else if (value.length != 8) {
                                            return 'Dados inválidos, CEP contém 8 dígitos!';
                                          }
                                          return null;
                                        },
                                        onChanged: (value) {
                                          setState(() {
                                            _cep = value;
                                          });
                                        },
                                        decoration: InputDecoration(
                                          labelText: 'CEP',
                                          labelStyle: TextStyle(
                                            color: const Color(0xff858798),
                                            fontSize: 14 * textScale,
                                          ),
                                          enabledBorder:
                                              const OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Color(0xff939393)),
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(5),
                                            ),
                                          ),
                                          focusedBorder:
                                              const OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Color.fromARGB(
                                                    255, 10, 78, 78)),
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(5),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),

                                //------
                                //States
                                Material(
                                  elevation: 5,
                                  shadowColor: const Color(0xff000040),
                                  borderRadius: BorderRadius.circular(5),
                                  child: Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(9, 0, 0, 0),
                                    child: DropdownButton(
                                      value: _cState.isEmpty ? null : _cState,
                                      onChanged: _updateDropDownState,
                                      items: const [
                                        DropdownMenuItem(
                                          value: 'AC',
                                          child: Text('AC'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'AL',
                                          child: Text('AL'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'AP',
                                          child: Text('AP'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'AM',
                                          child: Text('AM'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'BA',
                                          child: Text('BA'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'CE',
                                          child: Text('CE'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'DF',
                                          child: Text('DF'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'ES',
                                          child: Text('ES'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'GO',
                                          child: Text('GO'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'MA',
                                          child: Text('MA'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'MT',
                                          child: Text('MT'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'MS',
                                          child: Text('MS'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'MG',
                                          child: Text('MG'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'PA',
                                          child: Text('PA'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'PB',
                                          child: Text('PB'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'PR',
                                          child: Text('PR'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'PE',
                                          child: Text('PE'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'PI',
                                          child: Text('PI'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'RJ',
                                          child: Text('RJ'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'RN',
                                          child: Text('RN'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'RS',
                                          child: Text('RS'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'RO',
                                          child: Text('RO'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'RR',
                                          child: Text('RR'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'SC',
                                          child: Text('SC'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'SP',
                                          child: Text('SP'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'SE',
                                          child: Text('SE'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'TO',
                                          child: Text('TO'),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),

                          //-------
                          //Description
                          Material(
                            elevation: 5,
                            shadowColor: const Color(0xff000040),
                            borderRadius: BorderRadius.circular(5),
                            child: SizedBox(
                              width: scrWidth * 0.78,
                              height: scrHeight * 0.30,
                              child: TextFormField(
                                keyboardType: TextInputType.multiline,
                                maxLines: 15,
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Favor preencher os dados';
                                  } else if (value.length < 6) {
                                    return 'Entrada inválida, mínimo 6 caracteres!';
                                  }
                                  return null;
                                },
                                onChanged: (value) {
                                  setState(() {
                                    _description = value;
                                  });
                                },
                                decoration: InputDecoration(
                                  labelText: 'Descrição profissional',
                                  labelStyle: TextStyle(
                                    color: const Color(0xff858798),
                                    fontSize: 14 * textScale,
                                  ),
                                  enabledBorder: const OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Color(0xff939393)),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(5),
                                    ),
                                  ),
                                  focusedBorder: const OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Color.fromARGB(255, 10, 78, 78)),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(5),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          const Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 0, vertical: 40),
                            child: RegistrationAreaTitle(
                                content:
                                    'Selecione os serviços que você deseja prestar...'),
                          ),
                          CategoryRegister(
                            isLoadingData: false,
                            selectedCategories: selectedCategories,
                          ),
                          //Util-Somente para ver os dados
                          // ElevatedButton(
                          //     onPressed: () => {print(selectedCategories)},
                          //     child: Text('Dados'))
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 32, 0, 40),
                      child: Button(
                          btnFunction: () => {
                                if (_formKey.currentState!.validate())
                                  {
                                    //Retira o foco do teclado de input
                                    FocusManager.instance.primaryFocus
                                        ?.unfocus(),
                                    // ignore: unnecessary_new
                                    newWorker = new Worker(
                                        id: 0,
                                        password: _passInput,
                                        title: _title,
                                        email: _email,
                                        phoneA: _phoneA,
                                        phoneB: _phoneB,
                                        cnpj: _CNPJ,
                                        cpf: _CPF,
                                        likes: 0,
                                        favorites: 0,
                                        services: 0,
                                        description: _description,
                                        avatar: '',
                                        cep: _cep,
                                        adress: _adress,
                                        neighborhood: _neighborhood,
                                        city: _city,
                                        state: _cState,
                                        isWorker: 1),

                                    //API-POST
                                    WorkerHandler()
                                        .addWorker(
                                            api: "newWorker",
                                            newWorker: newWorker)
                                        .then((resp) {
                                      if (resp['statusCode'] == 201) {
                                        _changeScreen(context);
                                      } else {
                                        debugPrint(bodyRespStringHandler(
                                            body: resp['bodyData'].toString()));

                                        final snackStatus = SnackBar(
                                          content: Text(bodyRespStringHandler(
                                              body:
                                                  resp['bodyData'].toString())),
                                          action: SnackBarAction(
                                            label: 'Ok',
                                            onPressed: () {
                                              // Some code to undo the change.
                                            },
                                          ),
                                        );
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(snackStatus);
                                      }
                                    }),
                                  }
                              },
                          btnText: 'Finalizar',
                          btnType: BtnType.defaultBtn),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
