import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:salva_obra/presentation/screens/login_screen.dart';
import 'package:salva_obra/presentation/widgets/registration_area_title.dart';
import 'dart:io';
import '../../widgets/button_widget.dart';

class RegisterConfirmationScreen extends StatelessWidget {
  final bool isClient;

  const RegisterConfirmationScreen({super.key, required this.isClient});

  void _changeScreen(BuildContext context) {
    //Check System type
    Navigator.of(context).push(Platform.isAndroid
        ? MaterialPageRoute(builder: (_) {
            return const LoginScreen();
          })
        : CupertinoPageRoute(builder: (_) {
            return const LoginScreen();
          }));
  }

  @override
  Widget build(BuildContext context) {
    final scrHeight = MediaQuery.of(context).size.height;

    //WillPopScope = Disable native back button
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Container(
        decoration: const BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255), // background color
          image: DecorationImage(
              image: AssetImage('lib/presentation/assets/screen_bg.png'),
              fit: BoxFit.cover), // background image above color
        ),
        child: SafeArea(
          child: Scaffold(
            backgroundColor: Colors.transparent,
            body: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: scrHeight * 0.05),
                  child: Image(
                    image: AssetImage(isClient
                        ? 'lib/presentation/assets/client_reg_finished.png'
                        : 'lib/presentation/assets/worker_reg_finished.png'),
                  ),
                ),
                Column(
                  children: <Widget>[
                    const RegistrationAreaTitle(
                        content: 'Cadastro finalizado com sucesso!'),
                    Padding(
                      padding: const EdgeInsets.only(top: 21),
                      child: Button(
                        btnText: 'Finalizar',
                        btnType: BtnType.selectUserTypeBtn,
                        btnFunction: () => {
                          _changeScreen(context),
                        },
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
