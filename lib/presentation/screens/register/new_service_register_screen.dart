import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:salva_obra/presentation/screens/home_screen.dart';
import 'package:salva_obra/presentation/screens/services_history_screen.dart';
import 'package:salva_obra/presentation/widgets/button_widget.dart';
import 'package:salva_obra/presentation/widgets/registration_area_title.dart';
import 'package:salva_obra/utils/work_categories.dart';

class NewServiceRegisterScreen extends StatefulWidget {
  const NewServiceRegisterScreen({super.key});

  @override
  State<NewServiceRegisterScreen> createState() =>
      _NewServiceRegisterScreenState();
}

class _NewServiceRegisterScreenState extends State<NewServiceRegisterScreen> {
  final _formKey = GlobalKey<FormState>();

  String _name = '';
  String _phone = '';
  String _cState = WorkCategories.pedreiro.name;
  String _cType = 'prestador';
  String _price = '0.0';

  void _changeScreen(BuildContext context) {
    //Check System type
    Navigator.of(context).pushReplacement(Platform.isAndroid
        ? MaterialPageRoute(builder: (_) {
            return const ServicesHistoryScreen();
          })
        : CupertinoPageRoute(builder: (_) {
            return const ServicesHistoryScreen();
          }));
  }

  void _updateDropDownState(String? value) {
    if (value is String) {
      setState(() {
        _cState = value;
      });
    }
  }

  void _updateDropDownStateServiceType(String? value) {
    if (value is String) {
      setState(() {
        _cType = value;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final scrWidth = MediaQuery.of(context).size.width;
    final scrHeight = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor;

    return Container(
      decoration: const BoxDecoration(
        color: Color.fromARGB(255, 255, 255, 255), // background color
        image: DecorationImage(
            image: AssetImage('lib/presentation/assets/screen_bg.png'),
            fit: BoxFit.cover), // background image above color
      ),
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            shadowColor: Colors.transparent,
            iconTheme: const IconThemeData(color: Color(0xff8F8F90), size: 30),
            centerTitle: true,
            title: Text(
              'Novo Serviço',
              style: TextStyle(
                  color: const Color(0xff4D4D4D),
                  fontSize: 24 * textScale,
                  fontWeight: FontWeight.w600,
                  shadows: const <Shadow>[
                    Shadow(
                        blurRadius: 4,
                        offset: Offset(0, 4),
                        color: Color.fromARGB(60, 0, 0, 0))
                  ]),
            ),
          ),
          backgroundColor: Colors.transparent,
          body: ListView(
            children: <Widget>[
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    //User form
                    Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          //------
                          //Service Type
                          const Padding(
                            padding: EdgeInsets.fromLTRB(0, 45, 0, 9),
                            child: RegistrationAreaTitle(
                                content: 'Tipo de serviço'),
                          ),
                          Material(
                            elevation: 5,
                            shadowColor: const Color(0xff000040),
                            borderRadius: BorderRadius.circular(5),
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(9, 0, 0, 0),
                              child: Container(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: DropdownButton(
                                  isExpanded: true,
                                  value: _cType.isEmpty ? null : _cType,
                                  onChanged: _updateDropDownStateServiceType,
                                  items: const [
                                    DropdownMenuItem(
                                      value: 'prestador',
                                      child: Text('prestador'),
                                    ),
                                    DropdownMenuItem(
                                      value: 'cliente',
                                      child: Text('cliente'),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          //------
                          //Category
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                            child: const RegistrationAreaTitle(
                                content: 'Categoria'),
                          ),
                          Material(
                            elevation: 5,
                            shadowColor: const Color(0xff000040),
                            borderRadius: BorderRadius.circular(5),
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(9, 0, 0, 0),
                              child: Container(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: DropdownButton(
                                  isExpanded: true,
                                  value: _cState.isEmpty ? null : _cState,
                                  onChanged: _updateDropDownState,
                                  items: [
                                    DropdownMenuItem(
                                      value: WorkCategories.pedreiro.name,
                                      child: Text(WorkCategories.pedreiro.name),
                                    ),
                                    DropdownMenuItem(
                                      value: WorkCategories.servente.name,
                                      child: Text(WorkCategories.servente.name),
                                    ),
                                    DropdownMenuItem(
                                      value: WorkCategories.arquitetura.name,
                                      child:
                                          Text(WorkCategories.arquitetura.name),
                                    ),
                                    DropdownMenuItem(
                                      value: WorkCategories.engenharia.name,
                                      child:
                                          Text(WorkCategories.engenharia.name),
                                    ),
                                    DropdownMenuItem(
                                      value: WorkCategories.encanamento.name,
                                      child:
                                          Text(WorkCategories.encanamento.name),
                                    ),
                                    DropdownMenuItem(
                                      value: WorkCategories.eletricista.name,
                                      child:
                                          Text(WorkCategories.eletricista.name),
                                    ),
                                    DropdownMenuItem(
                                      value: WorkCategories.marcenaria.name,
                                      child:
                                          Text(WorkCategories.marcenaria.name),
                                    ),
                                    DropdownMenuItem(
                                      value: WorkCategories.frete.name,
                                      child: Text(WorkCategories.frete.name),
                                    ),
                                    DropdownMenuItem(
                                      value: WorkCategories.demolicao.name,
                                      child:
                                          Text(WorkCategories.demolicao.name),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          //------
                          //name
                          Padding(
                            padding: const EdgeInsets.only(top: 40),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Favor preencher os dados';
                                    } else if (value.length < 2) {
                                      return 'Dados inválidos, mínimo 2 caracteres!';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _name = value;
                                    });
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'Nome',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          //----
                          //Phone - A
                          Padding(
                            padding: const EdgeInsets.only(top: 33),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Favor preencher os dados';
                                    } else if (value.length < 9 ||
                                        value.length > 12) {
                                      return 'Numero de telefone inválido.';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _phone = value;
                                    });
                                  },
                                  keyboardType: TextInputType.phone,
                                  decoration: InputDecoration(
                                    labelText: 'Telefone',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          //----
                          //Price
                          Padding(
                            padding: const EdgeInsets.only(top: 33),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Favor preencher os dados';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _price = value;
                                    });
                                  },
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                    labelText: 'Valor',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 150),
                            child: Button(
                                btnText: 'Próximo',
                                btnFunction: () => {
                                      if (_formKey.currentState!.validate())
                                        {_changeScreen(context)}
                                    },
                                btnType: BtnType.defaultBtn),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
