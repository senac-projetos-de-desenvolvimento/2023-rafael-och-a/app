import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:salva_obra/domain/entities/customer.dart';
import 'package:salva_obra/domain/repositories/customer_handler.dart';
import 'dart:io';

import 'package:salva_obra/presentation/widgets/registration_area_title.dart';
import 'package:salva_obra/utils/reg_ex_validators.dart';
import '../../../utils/body_resp_string_handler.dart';
import '../../widgets/button_widget.dart';

import 'package:salva_obra/presentation/screens/register/register_confirmation_screen.dart';

class ClientRegister extends StatefulWidget {
  const ClientRegister({super.key});

  @override
  State<ClientRegister> createState() => _ClientRegister();
}

class _ClientRegister extends State<ClientRegister> {
  final _formKey = GlobalKey<FormState>();
  final validators = const RegExValidators();

  String _passInput = '';
  String _confPassInput = '';
  String _email = '';
  String _confEmail = '';

  String _firstName = '';
  String _lastName = '';
  String _phone = '';
  String _adress = '';
  String _neighborhood = '';
  String _city = '';
  String _cep = '';
  String _cState = 'RS';

  //Late - API addition controller
  late Customer newCustomer;

  void _changeScreen(BuildContext context) {
    //Check System type
    Navigator.of(context).pushReplacement(Platform.isAndroid
        ? MaterialPageRoute(builder: (_) {
            return const RegisterConfirmationScreen(isClient: true);
          })
        : CupertinoPageRoute(builder: (_) {
            return const RegisterConfirmationScreen(isClient: true);
          }));
  }

  void _updateDropDownState(String? value) {
    if (value is String) {
      setState(() {
        _cState = value;
      });
    }
  }

  bool _validateEmail({required String email}) {
    return RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(email);
  }

  @override
  Widget build(BuildContext context) {
    final scrWidth = MediaQuery.of(context).size.width;
    final scrHeight = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor;

    //Criar as telas de cadastro de cliente e prestador e chamar aqui....

    return Container(
      decoration: const BoxDecoration(
        color: Color.fromARGB(255, 255, 255, 255), // background color
        image: DecorationImage(
            image: AssetImage('lib/presentation/assets/screen_bg.png'),
            fit: BoxFit.cover), // background image above color
      ),
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            shadowColor: Colors.transparent,
            iconTheme: const IconThemeData(color: Color(0xff8F8F90), size: 30),
            centerTitle: true,
            title: Text(
              'Cliente',
              style: TextStyle(
                  color: const Color(0xff4D4D4D),
                  fontSize: 24 * textScale,
                  fontWeight: FontWeight.w600,
                  shadows: const <Shadow>[
                    Shadow(
                        blurRadius: 4,
                        offset: Offset(0, 4),
                        color: Color.fromARGB(60, 0, 0, 0))
                  ]),
            ),
          ),
          backgroundColor: Colors.transparent,
          body: ListView(
            children: <Widget>[
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    const Padding(
                      padding: EdgeInsets.fromLTRB(0, 50, 0, 31),
                      child: RegistrationAreaTitle(
                          content: 'Informações da conta'),
                    ),
                    //User form
                    Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          //----
                          //E-mail
                          Padding(
                            padding: const EdgeInsets.only(top: 5),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Favor preencher os dados';
                                    } else if (value.length < 7) {
                                      return 'Dados inválidos, mínimo 7 caracteres!';
                                    }
                                    if (validators.validateEmail(
                                            email: _email) ==
                                        false) {
                                      return 'Informe um e-mail válido';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _email = value;
                                    });
                                  },
                                  keyboardType: TextInputType.emailAddress,
                                  decoration: InputDecoration(
                                    labelText: 'E-mail',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          //----
                          //Conf.E-mail
                          Padding(
                            padding: const EdgeInsets.only(top: 13),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Favor preencher os dados';
                                    } else if (value.length < 7) {
                                      return 'Dados inválidos, mínimo 7 caracteres!';
                                    } else if (_confEmail != _email) {
                                      return 'Favor preencher com o mesmo e-mail';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _confEmail = value;
                                    });
                                  },
                                  keyboardType: TextInputType.emailAddress,
                                  decoration: InputDecoration(
                                    labelText: 'Confirmar E-mail',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          //--------
                          //Password
                          Padding(
                            padding: const EdgeInsets.only(top: 33),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Favor preencher os dados';
                                    } else if (value.length < 6) {
                                      return 'Senha inválida, mínimo 6 caracteres!';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _passInput = value;
                                    });
                                  },
                                  obscureText: true,
                                  decoration: InputDecoration(
                                    labelText: 'Senha',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          //-----------
                          //Conf.Password
                          Padding(
                            padding: const EdgeInsets.only(top: 13),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Favor preencher os dados';
                                    } else if (value.length < 6) {
                                      return 'Senha inválida, mínimo 6 caracteres!';
                                    } else if (_confPassInput != _passInput) {
                                      return 'As senhas devem ser iguais.';
                                    }

                                    return null;
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _confPassInput = value;
                                    });
                                  },
                                  obscureText: true,
                                  decoration: InputDecoration(
                                    labelText: 'Confirmar senha',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          const Padding(
                            padding: EdgeInsets.fromLTRB(0, 50, 0, 31),
                            child: RegistrationAreaTitle(
                                content: 'Informações pessoais'),
                          ),

                          //------
                          //F.Name
                          Padding(
                            padding: const EdgeInsets.only(top: 13),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Favor preencher os dados';
                                    } else if (value.length < 2) {
                                      return 'Dados inválidos, mínimo 2 caracteres!';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _firstName = value;
                                    });
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'Nome',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          //------
                          //L.Name
                          Padding(
                            padding: const EdgeInsets.only(top: 13),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Favor preencher os dados';
                                    } else if (value.length < 2) {
                                      return 'Dados inválidos, mínimo 2 caracteres!';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _lastName = value;
                                    });
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'Sobrenome',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          //----
                          //Phone
                          Padding(
                            padding: const EdgeInsets.only(top: 33),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Favor preencher os dados';
                                    } else if (value.length < 9 ||
                                        value.length > 11) {
                                      return 'Numero de telefone inválido.';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _phone = value;
                                    });
                                  },
                                  keyboardType: TextInputType.phone,
                                  decoration: InputDecoration(
                                    labelText: 'Telefone',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          //------
                          //Adress
                          Padding(
                            padding: const EdgeInsets.only(top: 33),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Favor preencher os dados';
                                    } else if (value.length < 5) {
                                      return 'Dados inválidos, mínimo 5 caracteres!';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _adress = value;
                                    });
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'Endereço',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          //------
                          //Neighborhood
                          Padding(
                            padding: const EdgeInsets.only(top: 13),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Favor preencher os dados';
                                    } else if (value.length < 2) {
                                      return 'Dados inválidos, mínimo 2 caracteres!';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _neighborhood = value;
                                    });
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'Bairro',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          //------
                          //City
                          Padding(
                            padding: const EdgeInsets.only(top: 13),
                            child: Material(
                              elevation: 5,
                              shadowColor: const Color(0xff000040),
                              borderRadius: BorderRadius.circular(5),
                              child: SizedBox(
                                width: scrWidth * 0.78,
                                height: scrHeight * 0.06,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Favor preencher os dados';
                                    } else if (value.length < 2) {
                                      return 'Dados inválidos, mínimo 2 caracteres!';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      _city = value;
                                    });
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'Cidade',
                                    labelStyle: TextStyle(
                                      color: const Color(0xff858798),
                                      fontSize: 14 * textScale,
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xff939393)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 10, 78, 78)),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          //----------
                          //CEP & State
                          SizedBox(
                            width: scrWidth * 0.78,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                //------
                                //CEP
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(0, 33, 15, 33),
                                  child: Material(
                                    elevation: 5,
                                    shadowColor: const Color(0xff000040),
                                    borderRadius: BorderRadius.circular(5),
                                    child: SizedBox(
                                      width: scrWidth * 0.43,
                                      height: scrHeight * 0.06,
                                      child: TextFormField(
                                        keyboardType: TextInputType.number,
                                        validator: (value) {
                                          if (value == null || value.isEmpty) {
                                            return 'Favor preencher os dados';
                                          } else if (value.length != 8) {
                                            return 'Dados inválidos, CEP contém 8 dígitos!';
                                          }
                                          return null;
                                        },
                                        onChanged: (value) {
                                          setState(() {
                                            _cep = value;
                                          });
                                        },
                                        decoration: InputDecoration(
                                          labelText: 'CEP',
                                          labelStyle: TextStyle(
                                            color: const Color(0xff858798),
                                            fontSize: 14 * textScale,
                                          ),
                                          enabledBorder:
                                              const OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Color(0xff939393)),
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(5),
                                            ),
                                          ),
                                          focusedBorder:
                                              const OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Color.fromARGB(
                                                    255, 10, 78, 78)),
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(5),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),

                                //------
                                //States
                                Material(
                                  elevation: 5,
                                  shadowColor: const Color(0xff000040),
                                  borderRadius: BorderRadius.circular(5),
                                  child: Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(9, 0, 0, 0),
                                    child: DropdownButton(
                                      value: _cState.isEmpty ? null : _cState,
                                      onChanged: _updateDropDownState,
                                      items: const [
                                        DropdownMenuItem(
                                          value: 'AC',
                                          child: Text('AC'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'AL',
                                          child: Text('AL'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'AP',
                                          child: Text('AP'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'AM',
                                          child: Text('AM'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'BA',
                                          child: Text('BA'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'CE',
                                          child: Text('CE'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'DF',
                                          child: Text('DF'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'ES',
                                          child: Text('ES'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'GO',
                                          child: Text('GO'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'MA',
                                          child: Text('MA'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'MT',
                                          child: Text('MT'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'MS',
                                          child: Text('MS'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'MG',
                                          child: Text('MG'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'PA',
                                          child: Text('PA'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'PB',
                                          child: Text('PB'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'PR',
                                          child: Text('PR'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'PE',
                                          child: Text('PE'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'PI',
                                          child: Text('PI'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'RJ',
                                          child: Text('RJ'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'RN',
                                          child: Text('RN'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'RS',
                                          child: Text('RS'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'RO',
                                          child: Text('RO'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'RR',
                                          child: Text('RR'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'SC',
                                          child: Text('SC'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'SP',
                                          child: Text('SP'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'SE',
                                          child: Text('SE'),
                                        ),
                                        DropdownMenuItem(
                                          value: 'TO',
                                          child: Text('TO'),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.only(bottom: 39),
                      child: Button(
                          btnFunction: () => {
                                if (_formKey.currentState!.validate())
                                  {
                                    //Retira o foco do teclado de input
                                    FocusManager.instance.primaryFocus
                                        ?.unfocus(),

                                    // ignore: unnecessary_new
                                    newCustomer = new Customer(
                                      id: 0,
                                      firstName: _firstName,
                                      lastName: _lastName,
                                      password: _passInput,
                                      phone: _phone,
                                      email: _email,
                                      favorites: 0,
                                      services: 0,
                                      cep: _cep,
                                      adress: _adress,
                                      neighborhood: _neighborhood,
                                      city: _city,
                                      state: _cState,
                                      avatar: '',
                                      isWorker: 0,
                                    ),

                                    //API-Post
                                    CustomerHandler()
                                        .addCustomer(
                                            api: "newCustomer",
                                            newCustomer: newCustomer)
                                        .then(
                                      (resp) {
                                        if (resp['statusCode'] == 201) {
                                          _changeScreen(context);
                                        } else {
                                          debugPrint(bodyRespStringHandler(
                                              body:
                                                  resp['bodyData'].toString()));

                                          final snackStatus = SnackBar(
                                            content: Text(bodyRespStringHandler(
                                                body: resp['bodyData']
                                                    .toString())),
                                            action: SnackBarAction(
                                              label: 'Ok',
                                              onPressed: () {
                                                // Some code to undo the change.
                                              },
                                            ),
                                          );
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(snackStatus);
                                        }
                                      },
                                    ),
                                  }
                              },
                          btnText: 'Finalizar',
                          btnType: BtnType.defaultBtn),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
