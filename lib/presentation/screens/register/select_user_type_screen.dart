import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:io';

import 'package:salva_obra/presentation/screens/register/client_register_screen.dart';
import 'package:salva_obra/presentation/screens/register/worker_register_screen.dart';
import 'package:salva_obra/presentation/widgets/button_widget.dart';

class SelectUserType extends StatefulWidget {
  const SelectUserType({super.key});

  @override
  State<SelectUserType> createState() => _SelectUserType();
}

class _SelectUserType extends State<SelectUserType> {
  bool _isWorkerType = false;

  void _setUserType() {
    setState(() {
      _isWorkerType = !_isWorkerType;
    });
  }

  void _changeScreen(BuildContext context) {
    //Check System type
    Navigator.of(context).push(Platform.isAndroid
        ? MaterialPageRoute(builder: (_) {
            return _isWorkerType
                ? const WorkerRegister()
                : const ClientRegister();
          })
        : CupertinoPageRoute(builder: (_) {
            return _isWorkerType
                ? const WorkerRegister()
                : const ClientRegister();
          }));
  }

  @override
  Widget build(BuildContext context) {
    final scrWidth = MediaQuery.of(context).size.width;
    final scrHeight = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor;

    //Criar as telas de cadastro de cliente e prestador e chamar aqui....

    return Container(
      decoration: const BoxDecoration(
        color: Color.fromARGB(255, 255, 255, 255), // background color
        image: DecorationImage(
            image: AssetImage('lib/presentation/assets/screen_bg.png'),
            fit: BoxFit.cover), // background image above color
      ),
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            shadowColor: Colors.transparent,
            iconTheme: const IconThemeData(color: Color(0xff8F8F90), size: 30),
          ),
          backgroundColor: Colors.transparent,
          body: ListView(
            children: <Widget>[
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    //Title
                    Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: scrHeight * 0.010,
                            horizontal: scrWidth * 0.06),
                        child: Text(
                          'Você é ...',
                          style: TextStyle(
                            color: const Color(0xff4D4D4D),
                            fontSize: 32 * textScale,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        _isWorkerType
                            ? Expanded(
                                child: GestureDetector(
                                  onTap: () => {_setUserType()},
                                  child: const Icon(
                                    Icons.arrow_left_rounded,
                                    size: 90,
                                  ),
                                ),
                              )
                            : const SizedBox(width: 90, height: 90),
                        Container(
                            margin: const EdgeInsets.symmetric(
                                vertical: 20, horizontal: 0),
                            width: scrWidth * 0.6,
                            height: scrHeight * 0.67,
                            alignment: Alignment.center,
                            child: Image(
                              image: AssetImage(_isWorkerType
                                  ? 'lib/presentation/assets/worker_character.png'
                                  : 'lib/presentation/assets/client_character.png'),
                            )),
                        !_isWorkerType
                            ? Expanded(
                                child: GestureDetector(
                                  onTap: () => {_setUserType()},
                                  child: const Icon(
                                    Icons.arrow_right_rounded,
                                    size: 90,
                                  ),
                                ),
                              )
                            : const SizedBox(
                                width: 90,
                                height: 90,
                              )
                      ],
                    ),
                    Button(
                        btnText: _isWorkerType ? 'Prestador' : 'Cliente',
                        btnType: BtnType.selectUserTypeBtn,
                        btnFunction: () => {_changeScreen(context)}),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
