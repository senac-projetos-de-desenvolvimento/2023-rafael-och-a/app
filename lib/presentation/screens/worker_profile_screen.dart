import 'package:flutter/material.dart';
import 'package:salva_obra/domain/repositories/worker_handler.dart';
import 'package:salva_obra/presentation/widgets/avatar_profile_area.dart';
import 'package:salva_obra/presentation/widgets/info_bar_widget.dart';
import 'package:salva_obra/presentation/widgets/profile_info_container.dart';

import '../../domain/entities/worker.dart';

class WorkerProfileScreen extends StatefulWidget {
  final int id;
  const WorkerProfileScreen({super.key, required this.id});

  @override
  State<WorkerProfileScreen> createState() => _WorkerProfileScreen();
}

class _WorkerProfileScreen extends State<WorkerProfileScreen> {
  late Future<Worker> worker;

  @override
  void initState() {
    super.initState();
    worker = WorkerHandler().getWorker(id: widget.id);
    debugPrint('\n------------------------------------------\n');
    debugPrint('Dados do prestador :: ${worker.toString()}');
  }

  @override
  Widget build(BuildContext context) {
    debugPrint('TESTE');
    final scrWidth = MediaQuery.of(context).size.width;
    final scrHeight = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor;

    return Container(
      decoration: const BoxDecoration(
        color: Color.fromARGB(255, 255, 255, 255), // background color
        image: DecorationImage(
            image: AssetImage('lib/presentation/assets/screen_bg.png'),
            fit: BoxFit.cover), // background image above color
      ),
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: const Color(0xff1F1F1F),
            shadowColor: Colors.transparent,
            toolbarHeight: 56,
            iconTheme: const IconThemeData(color: Color(0xff8F8F90), size: 30),
          ),
          backgroundColor: Colors.transparent,
          body: ListView(
            children: <Widget>[
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Stack(
                      alignment: Alignment.topCenter,
                      children: <Widget>[
                        Container(
                          width: scrWidth * 0.81,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                                color: const Color.fromARGB(255, 224, 224, 224),
                                width: 1),
                            borderRadius: const BorderRadius.all(
                              Radius.circular(7),
                            ),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              const SizedBox(
                                height: 200,
                              ),
                              FutureBuilder(
                                future: worker,
                                builder: (context, snapshot) {
                                  if (snapshot.connectionState ==
                                      ConnectionState.done) {
                                    if (snapshot.hasError) {
                                      debugPrint(snapshot.error.toString());
                                      return const Text(
                                          "ERRO!\nDados não puderam ser carregados");
                                    }
                                    return Column(
                                      children: <Widget>[
                                        InfoBar(
                                          isWorkerType: true,
                                          likes: snapshot.data!.likes,
                                          favorites: snapshot.data!.favorites,
                                          services: snapshot.data!.services,
                                        ),
                                        ProfileInfoContainer(
                                          isWorker: true,
                                          email: snapshot.data!.email,
                                          phoneA: snapshot.data!.phoneA,
                                          phoneB: snapshot.data!.phoneB,
                                          cep: snapshot.data!.cep,
                                          adress: snapshot.data!.adress,
                                          neighborhood:
                                              snapshot.data!.neighborhood,
                                          description:
                                              snapshot.data!.description,
                                        ),
                                      ],
                                    );
                                  } else {
                                    //Aqui também podemos usar um if
                                    //de ConnectionState.waiting
                                    return const CircularProgressIndicator();
                                  }
                                },
                              ),
                            ],
                          ),
                        ),
                        //----------------------------------------------------------
                        //Avatar + SubAppbar-Dark-Area
                        Stack(
                          children: <Widget>[
                            Container(
                              width: scrWidth,
                              height: 72,
                              decoration: BoxDecoration(
                                color: const Color(0xff1F1F1F),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 1,
                                    blurRadius: 4,
                                    offset: const Offset(0, 3),
                                  ),
                                ],
                              ),
                            ),
                            //Aqui vem um novo futureBuilder
                            FutureBuilder(
                              future: worker,
                              builder: (context, snapshot) {
                                if (snapshot.connectionState ==
                                    ConnectionState.done) {
                                  if (snapshot.hasError) {
                                    debugPrint(snapshot.error.toString());

                                    Container(
                                      alignment: Alignment.center,
                                      child: const AvatarProfileArea(
                                        avatar: 'default_client_avatar',
                                        name: '-',
                                        city: '-',
                                        state: '-',
                                      ),
                                    );
                                  }

                                  return Container(
                                    alignment: Alignment.center,
                                    child: AvatarProfileArea(
                                      avatar: 'default_client_avatar',
                                      name: snapshot.data!.title,
                                      city: snapshot.data!.city,
                                      state: snapshot.data!.state,
                                    ),
                                  );
                                } else {
                                  //Aqui também podemos usar um if
                                  //de ConnectionState.waiting
                                  return const CircularProgressIndicator();
                                }
                              },
                            ),
                          ],
                        ),
                      ],
                    ),

                    //----------------------------------------------------------
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
