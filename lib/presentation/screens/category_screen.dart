import 'package:flutter/material.dart';
import 'package:salva_obra/presentation/widgets/category_button.dart';
import 'package:salva_obra/utils/work_categories.dart';

class CategoryScreen extends StatefulWidget {
  const CategoryScreen({super.key});

  @override
  State<CategoryScreen> createState() => _CategoryScreen();
}

class _CategoryScreen extends State<CategoryScreen> {
  @override
  Widget build(BuildContext context) {
    final scrWidth = MediaQuery.of(context).size.width;
    final scrHeight = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor;

    return Container(
      decoration: const BoxDecoration(
        color: Color.fromARGB(255, 255, 255, 255), // background color
        image: DecorationImage(
            image: AssetImage('lib/presentation/assets/screen_bg.png'),
            fit: BoxFit.cover), // background image above color
      ),
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            shadowColor: Colors.transparent,
            iconTheme: const IconThemeData(color: Color(0xff8F8F90), size: 30),
            centerTitle: true,
            title: Text(
              'Categorias',
              style: TextStyle(
                color: const Color(0xff4D4D4D),
                fontSize: 24 * textScale,
                fontWeight: FontWeight.w600,
                shadows: const <Shadow>[
                  Shadow(
                      blurRadius: 4,
                      offset: Offset(0, 4),
                      color: Color.fromARGB(60, 0, 0, 0))
                ],
              ),
            ),
          ),
          backgroundColor: Colors.transparent,
          body: ListView(
            children: <Widget>[
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    //----------------------------------------------------------
                    //Title ----------------------------------------------------
                    Padding(
                      padding: const EdgeInsets.only(top: 30.0),
                      child: Text(
                        'Selecione a categoria de serviço\n que você deseja contratar   ',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          color: const Color(0xff6D6D6D),
                          fontSize: 16 * textScale,
                          fontWeight: FontWeight.w500,
                          shadows: const <Shadow>[
                            Shadow(
                              blurRadius: 4,
                              offset: Offset(0, 4),
                              color: Color.fromARGB(40, 0, 0, 0),
                            )
                          ],
                        ),
                      ),
                    ),

                    //----------------------------------------------------------
                    //Buttons --------------------------------------------------
                    Padding(
                      padding: const EdgeInsets.only(bottom: 30),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 7.5),
                            child: Column(
                              children: const <Widget>[
                                CategoryButton(
                                  category: WorkCategories.pedreiro,
                                  categoryId: 1,
                                ),
                                CategoryButton(
                                  category: WorkCategories.arquitetura,
                                  categoryId: 3,
                                ),
                                CategoryButton(
                                  category: WorkCategories.encanamento,
                                  categoryId: 5,
                                ),
                                CategoryButton(
                                  category: WorkCategories.marcenaria,
                                  categoryId: 7,
                                ),
                                CategoryButton(
                                  category: WorkCategories.demolicao,
                                  categoryId: 9,
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 7.5),
                            child: Column(
                              children: const <Widget>[
                                CategoryButton(
                                  category: WorkCategories.servente,
                                  categoryId: 2,
                                ),
                                CategoryButton(
                                  category: WorkCategories.engenharia,
                                  categoryId: 4,
                                ),
                                CategoryButton(
                                  category: WorkCategories.eletricista,
                                  categoryId: 6,
                                ),
                                CategoryButton(
                                  category: WorkCategories.frete,
                                  categoryId: 8,
                                ),
                                CategoryButton(
                                  category: WorkCategories.favoritos,
                                  categoryId: 10,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
