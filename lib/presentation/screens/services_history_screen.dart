import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:salva_obra/domain/gbl_data.dart' as globals;
import 'package:salva_obra/domain/repositories/service_handler.dart';
import 'package:salva_obra/presentation/screens/register/new_service_register_screen.dart';
import 'package:salva_obra/presentation/widgets/service_info_container.dart';
import 'package:salva_obra/utils/fix_date_format.dart';
import 'package:salva_obra/utils/work_categories.dart';

import '../../domain/entities/services.dart';

class ServicesHistoryScreen extends StatefulWidget {
  const ServicesHistoryScreen({super.key});

  @override
  State<ServicesHistoryScreen> createState() => _ServicesHistoryScreen();
}

class _ServicesHistoryScreen extends State<ServicesHistoryScreen> {
  late Future<List<Service>> userServices;

  void _changeScreen(BuildContext context) {
    //Check System type
    Navigator.of(context).pushReplacement(Platform.isAndroid
        ? MaterialPageRoute(builder: (_) {
            return const NewServiceRegisterScreen();
          })
        : CupertinoPageRoute(builder: (_) {
            return const NewServiceRegisterScreen();
          }));
  }

  @override
  void initState() {
    super.initState();
    userServices = ServiceHandler().userServices(userId: globals.id);
    debugPrint('\n\n------------------------------------------\n');
    debugPrint('Dados de serviço do usuário :: ${globals.userName}');
    debugPrint(userServices.toString());
  }

  @override
  Widget build(BuildContext context) {
    final scrWidth = MediaQuery.of(context).size.width;
    final scrHeight = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor;

    return Container(
      decoration: const BoxDecoration(
        color: Color.fromARGB(255, 255, 255, 255), // background color
        image: DecorationImage(
            image: AssetImage('lib/presentation/assets/screen_bg.png'),
            fit: BoxFit.cover), // background image above color
      ),
      child: SafeArea(
        child: Scaffold(
          floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
          floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.grey,
            onPressed: () => {_changeScreen(context)},
            tooltip: 'Increment',
            child: const Icon(Icons.add),
          ),
          bottomSheet: Container(
            color: const Color(0xff1E1F29),
            width: scrWidth,
            height: 56,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                //--------------------------------------------------------------
                //Profit
                Row(
                  children: <Widget>[
                    const Icon(
                      Icons.arrow_upward,
                      size: 22,
                      color: Color(0xff5DD88E),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 3),
                      child: Text(
                        '145,00',
                        style: TextStyle(
                          color: const Color(0xff5DD88E),
                          fontSize: 15 * textScale,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ],
                ),

                //--------------------------------------------------------------
                //Waste
                Row(
                  children: <Widget>[
                    const Icon(
                      Icons.arrow_downward,
                      size: 22,
                      color: Color(0xffD85D73),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 3),
                      child: Text(
                        '80,00',
                        style: TextStyle(
                          color: const Color(0xffD85D73),
                          fontSize: 15 * textScale,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ],
                ),

                //--------------------------------------------------------------
                //Balance
                Row(
                  children: <Widget>[
                    const Icon(
                      Icons.local_atm,
                      size: 22,
                      color: Color(0xffDBDD7F),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 3),
                      child: Text(
                        '65,00',
                        style: TextStyle(
                          color: const Color(0xffDBDD7F),
                          fontSize: 15 * textScale,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          appBar: AppBar(
            backgroundColor: const Color(0xff1E1F29),
            shadowColor: const Color(0xff1F1F1F),
            iconTheme: const IconThemeData(color: Color(0xff8F8F90), size: 30),
            centerTitle: true,
            title: Text(
              'Serviços ',
              style: TextStyle(
                color: const Color(0xffF5F5F5),
                fontSize: 20 * textScale,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          backgroundColor: Colors.transparent,
          body: ListView(
            children: <Widget>[
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: scrWidth * 0.81,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                            color: const Color.fromARGB(255, 224, 224, 224),
                            width: 1),
                        borderRadius: const BorderRadius.all(
                          Radius.circular(7),
                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          const SizedBox(
                            height: 30,
                          ),
                          FutureBuilder(
                            future: userServices,
                            builder: (context, snapshot) {
                              if (snapshot.connectionState ==
                                  ConnectionState.done) {
                                if (snapshot.hasError) {
                                  debugPrint(snapshot.error.toString());
                                  return const Text(
                                      "ERRO!\nDados não puderam ser carregados");
                                }
                                return SizedBox(
                                  width: scrWidth,
                                  height: scrHeight,
                                  child: ListView.builder(
                                    itemCount: snapshot.data!.length,
                                    itemBuilder: (context, index) {
                                      return ServiceInfoContainer(
                                          category: WorkCategories.values
                                              .elementAt(snapshot
                                                  .data![index].categoryId),
                                          author: snapshot.data![index].name,
                                          phone: '-----------',
                                          price: snapshot.data![index].price,
                                          date: fixDateFormat(
                                              date: snapshot
                                                  .data![index].creationDate),
                                          isProfit: snapshot.data![index]
                                                      .isProvided ==
                                                  0
                                              ? false
                                              : true);
                                    },
                                  ),
                                );
                              } else {
                                //Aqui também podemos usar um if
                                //de ConnectionState.waiting
                                return const CircularProgressIndicator();
                              }
                            },
                          ),
                          // --------------------------------------------------
                          //ServiceInfoContainer (start)
                          ServiceInfoContainer(
                            author:
                                'RafaelRafaelRafaelRafaelRafaelRafaelRafaelRafaelRafael',
                            date: '11/03/1994',
                            phone: '53991653099',
                            price: 15.0,
                            category: WorkCategories.values.elementAt(2),
                            isProfit: true,
                          ),
                          const SizedBox(
                            height: 70,
                          ),

                          //ServiceInfoContainer (end)
                          //----------------------------------------------------
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
