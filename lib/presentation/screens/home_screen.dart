import 'package:flutter/material.dart';
import 'package:salva_obra/presentation/widgets/info_bar_widget.dart';
import 'package:salva_obra/presentation/widgets/central_home_buttons.dart';
import 'package:salva_obra/utils/text_overflow_controll.dart';

import '../../domain/gbl_data.dart' as globals;

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreen();
}

class _HomeScreen extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    final scrWidth = MediaQuery.of(context).size.width;
    final scrHeight = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor;

    final String userName = globals.userName;

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xff1F1F1F),
          shadowColor: const Color(0xff1F1F1F),
          toolbarHeight: 56,
          iconTheme: const IconThemeData(color: Color(0xff8F8F90), size: 30),
        ),
        backgroundColor: const Color(0xffE9E9E9),
        body: ListView(
          children: <Widget>[
            //------------------------------------------------------------------
            //Topo -------------------------------------------------------------
            Container(
              height: scrHeight * 0.25,
              width: scrWidth,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 1,
                    blurRadius: 4,
                    offset: const Offset(0, 3),
                  ),
                ],
                color: const Color.fromARGB(
                    255, 255, 255, 255), // background color
                image: const DecorationImage(
                  image: AssetImage('lib/presentation/assets/screen_bg.png'),
                  fit: BoxFit.cover,
                ), // background image above color
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Bem-vindo,\n${textOverflowControll(maxSize: 7, text: userName)}',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        fontSize: 36 * textScale, fontWeight: FontWeight.w600),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.amber),
                      width: 75,
                      height: 85,
                    ),
                  )
                ],
              ),
            ),
            //------------------------------------------------------------------
            //Parte de baixo ---------------------------------------------------
            SizedBox(
              height: scrHeight * 0.65,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Column(
                    children: const [
                      CentralHomeButton(btnType: HomeBtnType.workers),
                      CentralHomeButton(btnType: HomeBtnType.services),
                      CentralHomeButton(btnType: HomeBtnType.profile),
                    ],
                  ),
                  InfoBar(
                    isWorkerType: globals.isWorker == 0 ? false : true,
                    services: globals.services,
                    likes: globals.likes,
                    favorites: globals.favorites,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
