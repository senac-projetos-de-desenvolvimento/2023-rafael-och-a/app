import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:salva_obra/domain/repositories/login_handler.dart';
import 'package:salva_obra/presentation/screens/home_screen.dart';
import 'package:salva_obra/presentation/widgets/button_widget.dart';
import 'package:salva_obra/presentation/screens/register/select_user_type_screen.dart';
import 'dart:io';

import 'package:salva_obra/utils/reg_ex_validators.dart';
import 'package:salva_obra/utils/show_globals.dart';

import '../../domain/entities/login_data.dart';
import '../../domain/gbl_data.dart' as globals;
import '../../utils/body_resp_string_handler.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreen();
}

class _LoginScreen extends State<LoginScreen> {
  String _emailInput = '';
  String _passInput = '';

  late LoginData loginData;

  final validators = const RegExValidators();

  final _formKey = GlobalKey<FormState>();

  void _changeScreen(BuildContext context) {
    //Check System type
    Navigator.of(context).push(Platform.isAndroid
        ? MaterialPageRoute(builder: (_) {
            return const SelectUserType();
          })
        : CupertinoPageRoute(builder: (_) {
            return const SelectUserType();
          }));
  }

  void _changeScreenHome(BuildContext context) {
    //Check System type
    Navigator.of(context).push(Platform.isAndroid
        ? MaterialPageRoute(builder: (_) {
            return const HomeScreen();
          })
        : CupertinoPageRoute(builder: (_) {
            return const HomeScreen();
          }));
  }

  @override
  Widget build(BuildContext context) {
    final scrWidth = MediaQuery.of(context).size.width;
    final scrHeight = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor;

    return Container(
        decoration: const BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255), // background color
          image: DecorationImage(
              image: AssetImage('lib/presentation/assets/login_bg.png'),
              fit: BoxFit.cover), // background image above color
        ),
        child: SafeArea(
          child: Scaffold(
              backgroundColor: Colors.transparent,
              body: ListView(
                children: <Widget>[
                  Center(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      //Title
                      Image.asset(
                        'lib/presentation/assets/title.png',
                        width: scrWidth * 0.72,
                        height: scrHeight * 0.62,
                      ),

                      Form(
                          key: _formKey,
                          child: Column(
                            children: <Widget>[
                              Material(
                                elevation: 5,
                                shadowColor: const Color(0xff000040),
                                borderRadius: BorderRadius.circular(5),
                                child: SizedBox(
                                  width: scrWidth * 0.78,
                                  child: TextFormField(
                                    keyboardType: TextInputType.emailAddress,
                                    validator: (value) {
                                      if (value == null || value.isEmpty) {
                                        return 'Favor preencher os dados';
                                      } else if (value.length < 7) {
                                        return 'Dados inválidos, mínimo 7 caracteres!';
                                      }
                                      if (validators.validateEmail(
                                              email: _emailInput) ==
                                          false) {
                                        return 'Informe um e-mail válido';
                                      }
                                      return null;
                                    },
                                    onChanged: (text) {
                                      setState(() {
                                        _emailInput = text;
                                      });
                                    },
                                    decoration: InputDecoration(
                                      labelText: 'E-mail',
                                      labelStyle: TextStyle(
                                          color: const Color(0xff858798),
                                          fontSize: 14 * textScale),
                                      border: InputBorder.none,
                                      contentPadding:
                                          const EdgeInsets.symmetric(
                                              horizontal: 8, vertical: 10),
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(13),
                                child: Material(
                                  elevation: 5,
                                  shadowColor: const Color(0xff000040),
                                  borderRadius: BorderRadius.circular(5),
                                  child: SizedBox(
                                    width: scrWidth * 0.78,
                                    child: TextFormField(
                                      validator: (value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Favor preencher os dados';
                                        } else if (value.length < 6) {
                                          return 'Senha inválida, mínimo 6 caracteres!';
                                        }

                                        return null;
                                      },
                                      onChanged: (pText) {
                                        setState(() {
                                          _passInput = pText;
                                        });
                                      },
                                      obscureText: true,
                                      decoration: InputDecoration(
                                        labelText: 'Senha',
                                        labelStyle: TextStyle(
                                            color: const Color(0xff858798),
                                            fontSize: 14 * textScale),
                                        border: InputBorder.none,
                                        contentPadding:
                                            const EdgeInsets.symmetric(
                                                horizontal: 8, vertical: 10),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )),

                      //Buttons areas
                      Button(
                          btnFunction: () => {
                                if (_formKey.currentState!.validate())
                                  {
                                    //Retira o foco do teclado de input
                                    FocusManager.instance.primaryFocus
                                        ?.unfocus(),

                                    LoginHandler().login(api: "login", login: {
                                      "email": _emailInput,
                                      "password": _passInput
                                    }).then((resp) {
                                      if (resp['statusCode'] == 200) {
                                        //Global adds
                                        globals.id = resp['bodyData']!.id;
                                        globals.userName =
                                            resp['bodyData']!.userName;
                                        globals.isWorker =
                                            resp['bodyData']!.isWorker;
                                        globals.likes = resp['bodyData']!.likes;
                                        globals.services =
                                            resp['bodyData']!.services;
                                        globals.favorites =
                                            resp['bodyData']!.favorites;
                                        globals.neighborhood =
                                            resp['bodyData']!.neighborhood;
                                        globals.city = resp['bodyData']!.city;
                                        globals.uState =
                                            resp['bodyData']!.state;
                                        showGlobalsData();
                                        _changeScreenHome(context);
                                      } else {
                                        debugPrint(bodyRespStringHandler(
                                            body: resp['bodyData'].toString()));

                                        final snackStatus = SnackBar(
                                          content: Text(bodyRespStringHandler(
                                              body:
                                                  resp['bodyData'].toString())),
                                          action: SnackBarAction(
                                            label: 'Ok',
                                            onPressed: () {
                                              // Some code to undo the change.
                                            },
                                          ),
                                        );
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(snackStatus);
                                      }
                                    }),
                                  }
                              },
                          btnText: 'Entrar',
                          btnType: BtnType.defaultBtn),
                      Container(
                        margin: const EdgeInsets.only(top: 20),
                        child: GestureDetector(
                          onTap: () => {_changeScreen(context)},
                          child: Text(
                            'Registrar-se',
                            style: TextStyle(
                                color: const Color(0xFF1E1F29),
                                fontSize: 13 * textScale,
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                      )
                    ],
                  )),
                ],
              )),
        ));
  }
}
