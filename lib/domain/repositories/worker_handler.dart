import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

import '../entities/worker.dart';
import '../../domain/gbl_data.dart' as globals;

class WorkerHandler {
  var workerClient = http.Client();
  //Quando utilizando host local e emulador, precisamos disso
  //ao invés de 'localhost'
  final String baseUrl = 'http://10.0.2.2:3000/';

  Future<Worker> getWorker({required int id}) async {
    final response = await http.get(Uri.parse('${baseUrl}worker/$id'));

    if (response.statusCode == 200 && response.contentLength! > 0) {
      debugPrint('\n\nResp-Específico :: \n ${response.body}');
      final d = json.decode(response.body);
      // ignore: unnecessary_new
      final Worker worker = new Worker(
        id: d['id'],
        password: d['password'],
        title: d['title'],
        email: d['email'],
        phoneA: d['phoneA'],
        phoneB: d['phoneB'],
        cnpj: d['cnpj'] ?? '-',
        cpf: d['cpf'] ?? '-',
        likes: d['likes'],
        favorites: d['favorites'],
        services: d['services'],
        description: d['description'],
        avatar: d['avatar'],
        cep: d['cep'],
        adress: d['adress'],
        neighborhood: d['neighborhood'],
        city: d['city'],
        state: d['state'],
        isWorker: d['isWorker'],
      );
      return worker;
    } else {
      throw Exception('Failed to load worker');
    }
  }

  Future<List<Worker>> listWorkers(String api) async {
    final response = await http.get(Uri.parse(baseUrl + api));

    if (response.statusCode == 200 && response.contentLength! > 0) {
      debugPrint('\n\nResp-Listaa \n:: ${response.body}');
      var workersList = <Worker>[];
      Iterable list = json.decode(response.body);
      workersList = list.map((model) => Worker.fromJson(model)).toList();

      return workersList;
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load ListWorkers');
    }
  }

  //Captura todos os prestadores de uma determinada categoria
  Future<List<Worker>> listWorkersByCat({required int categoryId}) async {
    debugPrint('\n\n>>> Parâmetros recebidos :: ');
    debugPrint(
        'Category ID  - ${categoryId} | City - ${globals.city} | State - ${globals.uState}');

    final response = await http.post(
      Uri.parse('${baseUrl}indexCityStateForApp'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8'
      },
      body: jsonEncode({
        "category_id": categoryId,
        "city": globals.city,
        "state": globals.uState
      }),
    );

    if (response.statusCode == 200) {
      var workersList = <Worker>[];
      Iterable list = json.decode(response.body);

      debugPrint('\n\nResp-Lista \n:: ${list}');

      if (list is List<dynamic>) {
        for (final d in list) {
          debugPrint('------Adicionando um novo a lista ------');
          debugPrint(d['title']);
          // ignore: unnecessary_new
          workersList.add(new Worker(
              id: d['id'],
              password: d['password'],
              title: d['title'],
              email: d['email'],
              phoneA: d['phoneA'],
              phoneB: d['phoneB'],
              cnpj: d['cnpj'] ?? '-',
              cpf: d['cpf'] ?? '-',
              likes: d['likes'],
              favorites: d['favorites'],
              services: d['services'],
              description: d['description'],
              avatar: d['avatar'],
              cep: d['cep'],
              adress: d['adress'],
              neighborhood: d['neighborhood'],
              city: d['city'],
              state: d['state'],
              isWorker: d['isWorker']));
        }
      }

      debugPrint('\n\nTamanho da lista \n:: ${workersList.length}');

      return workersList;
    } else {
      debugPrint('\n\nERRO');
      debugPrint(response.statusCode.toString());
      debugPrint(response.body.toString());

      throw Exception('Failed to load ListWorkers');
    }
  }

  //Captura todos os prestadores de uma determinada categoria
  Future<List<Worker>> listWorkersByCatAndNeighborhood(
      {required int categoryId}) async {
    debugPrint('\n\n>>> Parâmetros recebidos :: ');
    debugPrint(
        'Category ID  - ${categoryId} | City - ${globals.city} | State - ${globals.uState} | Neighborhood - ${globals.neighborhood}');

    final response = await http.post(
      Uri.parse('${baseUrl}indexNeighborhoodForApp'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8'
      },
      body: jsonEncode({
        "category_id": categoryId,
        "city": globals.city,
        "state": globals.uState,
        "neighborhood": globals.neighborhood
      }),
    );

    if (response.statusCode == 200) {
      var workersList = <Worker>[];
      Iterable list = json.decode(response.body);

      debugPrint('\n\nResp-Lista \n:: ${list}');

      if (list is List<dynamic>) {
        for (final d in list) {
          debugPrint('------Adicionando um novo a lista ------');
          debugPrint(d['title']);
          // ignore: unnecessary_new
          workersList.add(new Worker(
              id: d['id'],
              password: d['password'],
              title: d['title'],
              email: d['email'],
              phoneA: d['phoneA'],
              phoneB: d['phoneB'],
              cnpj: d['cnpj'] ?? '-',
              cpf: d['cpf'] ?? '-',
              likes: d['likes'],
              favorites: d['favorites'],
              services: d['services'],
              description: d['description'],
              avatar: d['avatar'],
              cep: d['cep'],
              adress: d['adress'],
              neighborhood: d['neighborhood'],
              city: d['city'],
              state: d['state'],
              isWorker: d['isWorker']));
        }
      }

      return workersList;
    } else {
      debugPrint('\n\nERRO');
      debugPrint(response.statusCode.toString());
      debugPrint(response.body.toString());

      throw Exception('Failed to load ListWorkers');
    }
  }

  Future<Map> addWorker({
    required String api,
    required Worker newWorker,
  }) async {
    final resp = await http.post(Uri.parse(baseUrl + api),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode({
          "password": newWorker.password,
          "title": newWorker.title,
          "email": newWorker.email,
          "phoneA": newWorker.phoneA,
          "phoneB": newWorker.phoneB.isEmpty ? '-' : newWorker.phoneB,
          "cnpj": newWorker.cnpj.isEmpty ? null : newWorker.cnpj,
          "cpf": newWorker.cpf.isEmpty ? null : newWorker.cpf,
          "likes": newWorker.likes,
          "favorites": newWorker.favorites,
          "services": newWorker.services,
          "description": newWorker.description,
          "avatar": newWorker.avatar.isEmpty ? '-' : newWorker.avatar,
          "cep": newWorker.cep,
          "adress": newWorker.adress,
          "neighborhood": newWorker.neighborhood,
          "city": newWorker.city,
          "state": newWorker.state
        }));

    if (resp.statusCode == 201) {
      debugPrint('WORKER REGISTERED !!!');
    } else {
      debugPrint(resp.body);
    }

    return {'statusCode': resp.statusCode, 'bodyData': resp.body};
  }
}
