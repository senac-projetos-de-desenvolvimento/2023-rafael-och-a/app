import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../entities/customer.dart';

class CustomerHandler {
  var customerClient = http.Client();
  final String baseUrl = 'http://10.0.2.2:3000/';

  //Método ainda não testado
  Future<Customer> getCustomer({required int id}) async {
    try {
      final response = await http.get(Uri.parse('${baseUrl}customer/$id'));

      if (response.statusCode == 200 && response.contentLength! > 0) {
        debugPrint('Customer Data :: ${response.body}');

        final d = json.decode(response.body);

        final customer = Customer(
            id: d[0]['id'],
            firstName: d[0]['firstName'],
            lastName: d[0]['lastName'],
            password: d[0]['password'],
            phone: d[0]['phone'],
            email: d[0]['email'],
            favorites: d[0]['favorites'],
            services: d[0]['services'],
            cep: d[0]['cep'],
            adress: d[0]['adress'],
            neighborhood: d[0]['neighborhood'],
            city: d[0]['city'],
            state: d[0]['state'],
            avatar: d[0]['avatar'],
            isWorker: d[0]['isWorker']);
        return customer;
      } else {
        throw Exception('Failed to load worker');
      }
    } catch (e) {
      debugPrint(e.toString());
      throw Exception('Failed to load worker :: ${e.toString()}');
    }
  }

  Future<Map> addCustomer({
    required String api,
    required Customer newCustomer,
  }) async {
    final resp = await http.post(
      Uri.parse(baseUrl + api),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode({
        "firstName": newCustomer.firstName,
        "lastName": newCustomer.lastName,
        "password": newCustomer.password,
        "phone": newCustomer.phone,
        "email": newCustomer.email,
        "cep": newCustomer.cep,
        "adress": newCustomer.adress,
        "neighborhood": newCustomer.neighborhood,
        "city": newCustomer.city,
        "state": newCustomer.state,
        "avatar": newCustomer.avatar.isEmpty ? '-' : newCustomer.avatar
      }),
    );
    if (resp.statusCode == 201) {
      debugPrint('CUSTOMER REGISTERED !!!');
    } else {
      debugPrint(resp.body);
    }

    return {'statusCode': resp.statusCode, 'bodyData': resp.body};
  }
}
