import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

import '../entities/login_data.dart';

class LoginHandler {
  var loginClient = http.Client();
  //Quando utilizando host local e emulador, precisamos disso
  //ao invés de 'localhost'
  final String baseUrl = 'http://10.0.2.2:3000/';

  Future<Map> login(
      {required String api, required Map<String, String> login}) async {
    final resp = await http.post(
      Uri.parse(baseUrl + api),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        {
          "email": login['email'],
          "password": login['password'],
        },
      ),
    );

    if (resp.statusCode == 200) {
      debugPrint('Login OK!!');
      return {
        'statusCode': resp.statusCode,
        'bodyData': LoginData.fromJson(jsonDecode(resp.body))
      };
    } else {
      debugPrint('Login Error :: ${resp.body}');
    }

    return {'statusCode': resp.statusCode, 'bodyData': resp.body};
  }
}
