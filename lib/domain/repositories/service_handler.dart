import 'dart:convert';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import '../entities/services.dart';

class ServiceHandler {
  final String baseUrl = 'http://10.0.2.2:3000';

  //Retorno de um serviço específico
  Future<Service> getService({required int id}) async {
    final resp = await http.get(Uri.parse('$baseUrl/service/$id'));

    if (resp.statusCode == 200 && resp.contentLength! > 0) {
      final data = json.decode(resp.body);

      // ignore: unnecessary_new
      return new Service(
          id: data['id'],
          name: data['name'],
          description: data['description'],
          price: data['price'],
          isProvided: data['isProvided'],
          categoryId: data['category_id'],
          workerId: data['worker_id'],
          creationDate: data['created_at']);
    } else {
      throw Exception('Failed to load service $id');
    }
  }

  //Listagem de serviços de um determinado usuário - Adicionar novo campo depois
  //que nesse caso é a string de titulo da categoria, mas isso só vira
  //quando houver a alteração no banco de dados

  //Adicionar futuramente método post
  Future<List<Service>> userServices({required int userId}) async {
    final resp = await http.get(Uri.parse('$baseUrl/workerServices/$userId'));
    if (resp.statusCode == 200) {
      List<Service> services = [];
      Iterable baseData = json.decode(resp.body);

      if (baseData is List<dynamic>) {
        for (final d in baseData) {
          // ignore: unnecessary_new
          services.add(new Service(
            id: d["id"],
            name: d["name"],
            description: d["description"],
            price: double.parse(d["price"]),
            isProvided: d["isProvided"],
            categoryId: d["category_id"],
            workerId: d["worker_id"],
            creationDate: d["creationDate"],
          ));
        }
      }

      return services;
    } else {
      throw Exception('Failed to load service $userId');
    }
  }

  //Remoção
  //Registro
  Future<int> serviceRegistration({required int userId}) async {
    return 0;
  }
}
