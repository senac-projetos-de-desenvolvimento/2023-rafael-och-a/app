class Service {
  final int id;
  final String name;
  final String description;
  final double price;
  final int isProvided;
  final int categoryId;
  final int workerId;
  final String creationDate;

  const Service({
    required this.id,
    required this.name,
    required this.description,
    required this.price,
    required this.isProvided,
    required this.categoryId,
    required this.workerId,
    required this.creationDate,
  });

  factory Service.fromJson(Map<String, dynamic> json) => Service(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        price: json["price"],
        isProvided: json["isProvided"],
        categoryId: json["category_id"],
        workerId: json["worker_id"],
        creationDate: json["creationDate"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "price": price,
        "isProvided": isProvided,
        "categoryId": categoryId,
        "workerId": workerId,
        "creationDate": creationDate
      };
}
