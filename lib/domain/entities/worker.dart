// To parse this JSON data, do
//
//     final worker = workerFromJson(jsonString);

import 'dart:convert';

List<Worker> listWorkerFromJson(String str) =>
    List<Worker>.from(json.decode(str).map((x) => Worker.fromJson(x)));
String listWorkerToJson(List<Worker> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

Worker workerFromJson(String str) => Worker.fromJson(json.decode(str));
String workerToJson(Worker data) => json.encode(data.toJson());

class Worker {
  int id;
  String password;
  String title;
  String email;
  String phoneA;
  String phoneB;
  String cnpj;
  String cpf;
  int likes;
  int favorites;
  int services;
  String description;
  String avatar;
  String cep;
  String adress;
  String neighborhood;
  String city;
  String state;
  int isWorker;

  Worker({
    required this.id,
    required this.password,
    required this.title,
    required this.email,
    required this.phoneA,
    required this.phoneB,
    required this.cnpj,
    required this.cpf,
    required this.likes,
    required this.favorites,
    required this.services,
    required this.description,
    required this.avatar,
    required this.cep,
    required this.adress,
    required this.neighborhood,
    required this.city,
    required this.state,
    required this.isWorker,
  });

  factory Worker.fromJson(Map<String, dynamic> json) => Worker(
        id: json["id"],
        password: json["password"],
        title: json["title"],
        email: json["email"],
        phoneA: json["phoneA"],
        phoneB: json["phoneB"],
        cnpj: json["cnpj"],
        cpf: json["cpf"],
        likes: json["likes"],
        favorites: json["favorites"],
        services: json["services"],
        description: json["description"],
        avatar: json["avatar"],
        cep: json["cep"],
        adress: json["adress"],
        neighborhood: json["neighborhood"],
        city: json["city"],
        state: json["state"],
        isWorker: json["isWorker"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "password": password,
        "title": title,
        "email": email,
        "phoneA": phoneA,
        "phoneB": phoneB,
        "cnpj": cnpj,
        "cpf": cpf,
        "likes": likes,
        "favorites": favorites,
        "services": services,
        "description": description,
        "avatar": avatar,
        "cep": cep,
        "adress": adress,
        "neighborhood": neighborhood,
        "city": city,
        "state": state,
        "isWorker": isWorker,
      };

  @override
  String toString() {
    return 'Worker Data::\nID:$id\nPSW:$password\nTITLE:$title\nEMAIL:$email\nPHONEa:$phoneA\nPHONEb$phoneB\n' +
        'CNPJ:$cnpj\nCPF:$cpf\nLIKES:$likes\nFAV:$favorites\nSEV: $services\nDESC:$description\nAV:$avatar\nCEP:$cep\nADR:$adress' +
        'NEIGH:$neighborhood\nCITY:$city\nSTATE:$state\nIW:$isWorker';
  }
}
