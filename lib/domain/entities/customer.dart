// To parse this JSON data, do
//
//     final customer = customerFromJson(jsonString);

import 'dart:convert';

Customer customerFromJson(String str) => Customer.fromJson(json.decode(str));
String customerToJson(Customer data) => json.encode(data.toJson());

List<Customer> listCustomerFromJson(String str) =>
    List<Customer>.from(json.decode(str).map((x) => Customer.fromJson(x)));
String listCustomerToJson(List<Customer> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Customer {
  int id;
  String firstName;
  String lastName;
  String password;
  String phone;
  String email;
  int favorites;
  int services;
  String cep;
  String adress;
  String neighborhood;
  String city;
  String state;
  String avatar;
  int isWorker;

  Customer({
    required this.id,
    required this.firstName,
    required this.lastName,
    required this.password,
    required this.phone,
    required this.email,
    required this.favorites,
    required this.services,
    required this.cep,
    required this.adress,
    required this.neighborhood,
    required this.city,
    required this.state,
    required this.avatar,
    required this.isWorker,
  });

  factory Customer.fromJson(Map<String, dynamic> json) => Customer(
        id: json["id"],
        firstName: json["firstName"],
        lastName: json["lastName"],
        password: json["password"],
        phone: json["phone"],
        email: json["email"],
        favorites: json["favorites"],
        services: json["services"],
        cep: json["cep"],
        adress: json["adress"],
        neighborhood: json["neighborhood"],
        city: json["city"],
        state: json["state"],
        avatar: json["avatar"],
        isWorker: json["isWorker"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "firstName": firstName,
        "lastName": lastName,
        "password": password,
        "phone": phone,
        "email": email,
        "favorites": favorites,
        "services": services,
        "cep": cep,
        "adress": adress,
        "neighborhood": neighborhood,
        "city": city,
        "state": state,
        "avatar": avatar,
        "isWorker": isWorker,
      };
}
