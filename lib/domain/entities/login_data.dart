import 'dart:convert';

LoginData loginFromJson({required String str}) =>
    LoginData.fromJson(json.decode(str));

class LoginData {
  int id;
  String userName;
  int isWorker;
  int services;
  int likes;
  int favorites;
  String neighborhood;
  String city;
  String state;

  LoginData({
    required this.id,
    required this.userName,
    required this.isWorker,
    required this.services,
    required this.likes,
    required this.favorites,
    required this.neighborhood,
    required this.city,
    required this.state,
  });

  factory LoginData.fromJson(Map<String, dynamic> json) => LoginData(
        id: json["id"],
        userName: json["title"] ?? json["firstName"],
        isWorker: json["isWorker"],
        services: json["services"],
        likes: json["likes"] ?? 0,
        favorites: json["favorites"],
        neighborhood: json["neighborhood"],
        city: json["city"],
        state: json["state"],
      );

  Map<String, dynamic> toJson() => {"id": id, "isWorker": isWorker};
}
